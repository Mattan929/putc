<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use common\models\CourseToSubject;
use yii\helpers\Html;

$this->title = 'Мои курсы';
$this->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['/lk']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= $this->title ?></h1>

<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
//        'user_id',
        [
            'attribute' => 'subject_id',
            'value' => function($model){
                return $model->subject->title;
            }
        ],
        [
            'attribute' => 'course_id',
            'value' => function($model){
                return $model->course->title;
            }
        ],
        [
            'attribute' => 'status',
            'value' => function($model){
                return $model->statusList[$model->status];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{action} {refuse} {info}',
            'buttons' => [
                'action' => function($url, $model, $key){
                    if ($model->status < \common\models\CourseToUser::STATUS_APPROVED){
                        $icon = '<i class="glyphicon glyphicon-paperclip"></i>';
                        return Html::a($icon . ' Документы', '#', [
                            'class' => 'label label-primary',
                            'title' => 'Документы',
                            'data-id' => $model->id,
                            'data-role' => 'documents-modal-open',
//                            'data-toggle' => 'modal',
                            'data-target' => '#documentsModal'
                        ]);
                    }
                    if ($model->status < \common\models\CourseToUser::STATUS_PAYMENT){
                        return Html::a('Оплатить', ['pay', 'id' => $model->id], [
                            'class' => 'label label-success',
                        ]);
                    }
                    if ($model->status === \common\models\CourseToUser::STATUS_PAYMENT){
                        $icon = '<i class="glyphicon glyphicon-new-window"></i>';
                        return Html::a($icon . ' Перейти к курсу', CourseToSubject::getCourseUrl($model->course_id, $model->subject_id), [
                            'class' => 'label label-default',
                            'target' => '_blank'
                        ]);
                    }
                },
                'refuse' => function($url, $model, $key){
                    if ($model->status < \common\models\CourseToUser::STATUS_APPROVED){
                        $icon = '<i class="glyphicon glyphicon-remove"></i>';
                        return Html::a($icon . ' Отказаться', ['refuse', 'id' => $model->id], [
                            'class' => 'label label-danger',
                        ]);
                    }
                },
                'info' => function($url, $model, $key){
                    if ($model->status < \common\models\CourseToUser::STATUS_APPROVED){
                        $info = Html::tag('i','', ['class' => 'glyphicon glyphicon-info-sign']);
                        return Html::a($info . ' Инфомрация', '#', [
                            'class' => 'label label-info',
                            'title' => 'Информация по записи',
                            'data-toggle' => 'modal',
                            'data-target' => '#informationModal'
                        ]);
                    }
                },
            ],
        ],
    ]
]);
?>

<div class="modal fade" id="informationModal" tabindex="-1" role="dialog" aria-labelledby="informationModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title text-center" id="informationModalLabel"><strong>Информация по записи</strong></h5>
            </div>
            <div class="modal-body">
                <?= $this->render('info') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="documentsModal" tabindex="-1" role="dialog" aria-labelledby="documentsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title text-center" id="documentsModalLabel">
                    <strong>Документы</strong>
                </h5>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-role="save-documents-form">Сохранить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
