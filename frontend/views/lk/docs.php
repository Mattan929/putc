<?php

use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model null|\common\models\CourseToUser */
/* @var $documents null|\common\models\Settings */

?>

<?php $form = ActiveForm::begin([
        'id' => 'documents-form',
        'options' => ['enctype' => 'multipart/form-data']
]); ?>
<div class="row">
    <div class="col-md-12">
        <label>Договор публичной оферты</label>
        <?= \floor12\files\components\FileListWidget::widget([
            'files' => [$documents->contract_offer]
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <?= $form->field($model, 'application')->widget(floor12\files\components\FileInputWidget::class) ?>
    </div>
    <div class="col-md-4">
        <label>Образец</label>
        <?= \floor12\files\components\FileListWidget::widget([
            'files' => [$documents->application]
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <?= $form->field($model, 'confirmation')->widget(floor12\files\components\FileInputWidget::class) ?>
    </div>
    <div class="col-md-4">
        <label>Образец</label>
        <?= \floor12\files\components\FileListWidget::widget([
                'files' => [$documents->confirmation]
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field(Yii::$app->user->identity, 'passport')->widget(floor12\files\components\FileInputWidget::class) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'check')->widget(floor12\files\components\FileInputWidget::class) ?>
    </div>
</div>


<?php ActiveForm::end(); ?>
