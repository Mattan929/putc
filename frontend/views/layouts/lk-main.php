<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="giy_CYD-u9NOudmE8OiI1eksHiNTAKUQFM1Cnk1UbMw"/>
    <meta name="yandex-verification" content="b8b9607f44b425e5"/>
    <?= $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]); ?>
    <?php
    $this->registerMetaTag(['name' => 'robots', 'content' => 'index, follow'], 'robots');
    $this->registerMetaTag(['name' => 'title', 'content' => 'ЦЕНТР ДОВУЗОВСКОЙ ПОДГОТОВКИ КГУ'], 'title');
    $this->registerMetaTag(['name' => 'h1', 'content' => 'ЦЕНТР ДОВУЗОВСКОЙ ПОДГОТОВКИ КГУ'], 'h1');
    $this->registerMetaTag(['name' => 'description', 'content' => 'ЦЕНТР ДОВУЗОВСКОЙ ПОДГОТОВКИ КГУ | ПОДГОТОВИТЕЛЬНЫЕ КУРСЫ | ПОДГОТОВКА К ЕГЭ | ПОДГОТОВИТЕЛЬНЫЕ КУРСЫ КУРГАН | ПОДГОТОВКА К ЕГЭ КУРГАН'], 'description');
    ?>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="header">
    <div class="pull-left logo-kgsu">
        <a target="_blank" href="http://kgsu.ru">
            <img width="75"
                 src="/images/logo.png"
                 alt="Курганский Государственный Университет">
        </a>
    </div>
    <div class="container">
        <div class="col-lg-3 header-block">
            <img width="50"
                 class="logo"
                 align="left"
                 src="/images/icons/icon.png"
                 alt="Курганский Государственный Университет">
            <div class="logo-text">
                <div class="header-9">
                    <strong>
                        КУРГАНСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ
                    </strong>
                </div>
                <div class="header-12">
                    <strong>
                        ЦЕНТР ДОВУЗОВСКОЙ ПОДГОТОВКИ
                    </strong>
                </div>
                <div class="header-15">
                    <strong>
                        ПОДГОТОВИТЕЛЬНЫЕ КУРСЫ
                    </strong>
                </div>
            </div>
        </div>
        <div class="col-lg-3 header-block">
            <div class="header-20 slogan-block">КАЖДОМУ ВЫПУСКНИКУ СТУДЕНЧЕСКИЙ БИЛЕТ</div>
        </div>
        <div class="col-lg-3 header-block">
            <a href="<?= Url::toRoute('/site/course') ?>" class="btn btn-putc">ПОДОБРАТЬ ПРОГРАММУ</a>
            <?php
            if (Yii::$app->user->isGuest) { ?>
                <a href="<?= Url::toRoute('/user/login') ?>" class="btn btn-default btn-login" title="Авторизоваться">
                    <i class="glyphicon glyphicon-log-in"></i>
                </a>
                <?php
            } else {
                echo Html::beginForm(['/user/security/logout'], 'post', ['style' => 'display: inline-block;'])
                    . Html::submitButton(
                        '<i class="glyphicon glyphicon-log-out"></i>',
                        ['class' => 'btn btn-default btn-login', 'title' => 'Выход']
                    )
                    . Html::endForm();
            }
            ?>
        </div>
        <div class="col-lg-3 header-block">
            <div class="text-right slogan-block">
                <a class="header-phone" href="tel:+73522654811">+7 (3522) <strong>654-811</strong></a>
                <a class="header-phone" href="tel:+73522462228">+7 (3522) <strong>462-228</strong></a>
                <br>
                <a class="header-11" target="_blank" href="http://kgsu.ru/sveden/common">СВЕДЕНИЯ ОБ ОБРАЗОВАТЕЛЬНОЙ
                    ОРГАНИЗАЦИИ</a>
                <br>
                <a class="header-11 pull-left" href="<?= Url::to('/lk') ?>" style="margin-left: 10px">ЛИЧНЫЙ КАБИНЕТ</a>
                <a class="header-11" target="_blank" href="http://pk.kgsu.ru">АБИТУРИЕНТУ</a>
            </div>
        </div>
    </div>
</div>
<div class="wrap">
    <?php
    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
//        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-putc',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
        ['label' => 'Новости и объявления', 'url' => ['/site/news']],
        ['label' => 'Информация по записи на курсы', 'url' => ['/page/index', 'slug' => 'informatsiya-po-zapisi-na-kursy']],
        ['label' => 'Расписания', 'url' => ['/site/schedule']],
        ['label' => 'Преподаватели', 'url' => ['/site/teacher']],
        ['label' => 'О нас', 'url' => ['/site/about']],
    ];
    if (Yii::$app->user->isGuest) {
//        $menuItems[] = ['label' => 'Регистрация', 'url' => ['/user/register']];
//        $menuItems[] = ['label' => 'Вход', 'url' => ['/user/security/login']];
    } else {
//        $menuItems[] = '<li>'
//            . Html::beginForm(['/user/security/logout'], 'post')
//            . Html::submitButton(
//                'Выйти (' . Yii::$app->user->identity->username . ')',
//                ['class' => 'btn btn-link logout']
//            )
//            . Html::endForm()
//            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?php if (isset($this->params['breadcrumbs'])) { ?>

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        <?php } ?>
        <?= Alert::widget() ?>

        <div class="row">
            <div class="col-md-2">
                <?= Nav::widget([
                    'options' => ['class' => 'nav-pills nav-stacked'],
                    'items' => [
                        ['label' => 'Профиль', 'url' => ['/user/settings']],
                        ['label' => 'Мои курсы', 'url' => ['/lk/my-course']],
                        ['label' => 'Информация по записи', 'url' => ['/lk/info']],
//                        ['label' => 'Расписание', 'url' => ['/lk/my-sсhedule']],
                    ],
                ]); ?>
            </div>
            <div class="col-md-10">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="first-line">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <img class="girl" src="/images/girl.png" height="709" alt="">
                </div>
                <div class="col-md-7">
                    <h2 class="text-left">ПОДБЕРИ <br>
                        ПРОГРАММУ ОБУЧЕНИЯ</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                    <a href="<?= Url::toRoute('course') ?>" class="btn btn-putc">Начать</a>
                </div>
            </div>
        </div>

    </div>
    <div class="second-line">
        <div class="container">
            <div class="row ">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                    <img width="100%"
                         class="logo"
                         align="left"
                         src="/images/logo-white.png"
                         alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8 text-center">
                    <h3>
                        КАЖДОМУ ВЫПУСКНИКУ<br>
                        СТУДЕНЧЕСКИЙ БИЛЕТ
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-right address">
                    г. Курган,<br>
                    ул. Пролетарская, 62, ауд. 245<br>
                    +7 (3522) 230-472,<br>
                    +7 (3522) 654-811<br>
                </div>
                <div class="col-md-4 text-left address">
                    г. Курган,<br>
                    ул. Советская, 63, ауд. 227<br>
                    +7 (3522) 462-228<br>
                    Е-mail: pk@kgsu.ru<br>
                </div>
            </div>
        </div>
    </div>

    <!--    <div class="line"></div>-->
</footer>

<?php $this->endBody() ?>
<script type="text/javascript">
    $(document).ready(function () {
        $(".fancybox").fancybox();
    });
</script>
</body>
</html>
<?php $this->endPage() ?>
