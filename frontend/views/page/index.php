<?php
/* @var $this yii\web\View
 * @var $model \common\models\Page
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page">
    <h1><?= $this->title ?></h1>

    <?= $model->content ?>
</div>

