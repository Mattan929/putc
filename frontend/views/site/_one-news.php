<?php

use yii\helpers\Url;

/** @var \common\models\Post $post */

$showMore = \yii\helpers\Html::a(' Читать полностью &#8594;', ['site/post', 'id' => $post->id]);
$mainImage = $post->post_image;
?>

<div class="row one-news">
    <div class="col-sm-12 col-md-4 col-lg-4 img-block">
        <a href="<?= Url::toRoute(['site/post', 'id' => $post->id]) ?>">
            <img width="100%" src="<?= $mainImage !== null ? $mainImage->href : '/images/icons/icon.png'?>"
                 alt="<?= $post->title ?>">
        </a>
    </div>
    <div class="col-sm-12 col-md-8 col-lg-8 text-block">
        <a href="<?= Url::toRoute(['site/post', 'id' => $post->id]) ?>">
            <h4><?= strip_tags($post->title) ?></h4>
        </a>
        <p>
            <?= strip_tags($post->description) . '<br>' . $showMore ?>
        </p>
    </div>
</div>
<hr>
