<?php

use yii\helpers\Url;

/** @var \common\models\Course $model
 * @var  \common\models\Subject $subject
 */

?>

<div class="row one-course">
    <h3 class="text-center"><?= mb_strtoupper($model->title) ?></h3>
    <div class="col-sm-12 col-md-3 col-lg-3">
        <label>Дата начала:</label> <?= Yii::$app->formatter->asDate($model->begin_date, 'dd MMMM Y') ?>
        <br>
        <label>Дата окончания:</label> <?= Yii::$app->formatter->asDate($model->end_date, 'dd MMMM Y') ?>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-2">
        <label> Предмет:</label>
        <?= $subject->title ?>
    </div>
    <div class="col-sm-12 col-md-2 col-lg-2">
        <label>Всего часов:</label> <?= $model->total_hours ?> ч.<br>
        <label>Часов в месяц:</label> <?= $model->hours_in_month ?> ч.<br>
        <label>Часов в неделю:</label> <?= $model->hours_in_week ?> ч.<br>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-3">
        <?php if (!empty($model->cost) && (integer)$model->cost > 0): ?>
        <label>Стоимость:</label> <?= $model->cost ?> р.<br>
        <label>Стоимость в месяц:</label> <?= round($model->cost / $model->total_hours, 2) * $model->hours_in_month ?>
        р.
        <label>Стоимость часа:</label> <?= round($model->cost / $model->total_hours, 2) ?> р.
        <?php else: ?>
        <label>Стоимость еще не указана</label>
        <?php endif; ?>
    </div>
    <div class="col-sm-12 col-md-2 col-lg-2">
        <?php
        if (!Yii::$app->user->isGuest) {
            $enroll = \common\models\CourseToUser::findOne(
                [
                    'user_id' => Yii::$app->user->identity->id,
                    'subject_id' => $subject->id,
                    'course_id' => $model->id
                ]
            );
            if ($enroll) {
                echo \yii\helpers\Html::tag('span', 'Запись оформлена', ['class' => 'label label-success']);
            } else {
                echo \yii\helpers\Html::a('Записаться', ['/lk/enroll-to-course', 'subjectId' => $subject->id, 'courseId' => $model->id], ['class' => 'btn btn-putc']);
            }
        } else {
            echo \yii\helpers\Html::a('Записаться', ['/lk/enroll-to-course', 'subjectId' => $subject->id, 'courseId' => $model->id], ['class' => 'btn btn-putc']);
        }
        ?>
    </div>
</div>
<hr>
