<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\helpers\Html;

$this->title = 'Новости и объявления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="site-news">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'summary' => false,
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_one-news', ['post' => $model]);
            },
            'pager' => [
                'class' => \kop\y2sp\ScrollPager::className(),
                'item' => '.item',
                'triggerText' => 'ПОКАЗАТЬ ЕЩЕ',
                'triggerTemplate' => '<div class="wrapper-show-more text-center ias-trigger" style="cursor: pointer"><a>{text}</a></div>',
                'noneLeftText' => 'Записей больше нет',]
        ]) ?>
    </div>
</div>