<?php

use yii\helpers\Url;

/** @var \common\models\Employee $model */
$mainImage = $model->photo;
?>

<div class="col-md-6">
    <div class="row one-employee">
        <div class="col-sm-12 col-md-5 col-lg-5 img-block">
            <img width="100%" src="<?= $mainImage !== null ? $mainImage->href : '/images/icons/icon.png' ?>"
                 alt="<?= $model->fullName ?>">
        </div>
        <div class="col-sm-12 col-md-7 col-lg-7 text-block">
            <h4><?= $model->fullName ?></h4>
            <p>
                <?= $model->description ?>
            </p>
            <p>
                <label>Должность:</label> <?= $model->position ?>
            </p>
            <label> Предметы:</label>
            <p>
                <?= implode(', ', \yii\helpers\ArrayHelper::getColumn($model->subjects, 'title')) ?>
            </p>
        </div>
    </div>
</div>