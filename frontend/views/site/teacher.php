<?php

/* @var $this \yii\web\View */
/* @var $dataProvider array|\common\models\Employee[]|\yii\db\ActiveRecord[] */

$this->title = 'Преподаватели';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="employee">

    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'summary' => false,
        'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('_one-employee', ['model' => $model]);
        },
        'pager' => [
            'class' => \kop\y2sp\ScrollPager::className(),
            'item' => '.item',
            'triggerText' => 'ПОКАЗАТЬ ЕЩЕ',
            'triggerTemplate' => '<div class="wrapper-show-more text-center ias-trigger" style="cursor: pointer"><a>{text}</a></div>',
            'noneLeftText' => '',
            ]
    ]) ?>

</div>
