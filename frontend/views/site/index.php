<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = Yii::$app->name . ' - главная';
?>
<div class="main-page">
    <h1 style="display: none"><?= Yii::$app->name ?></h1>
    <div class="slider">
        <div class="container">
            <?= \common\widgets\ContactFormWidget\ContactFormWidget::widget([
                'scenario' => 'consultation',
                'theme' => 'Записывайся на консультацию',
                'send_to' => \common\models\Settings::getValue('consultation-e-mail')
            ]) ?>

            <?php dominus77\owlcarousel2\WrapCarousel::begin([
                'id' => 'slider-carousel',
                'clientOptions' => [
                    'loop' => true,
                    'margin' => 800,
                    'nav' => false,
                    'dots' => false,
                    'autoplay' => false,
                    'autoplayTimeout' => 10000,
                    'autoplayHoverPause' => true,
                    'autoWidth' => true,
                    'responsive' => [
                        0 => [
                            'items' => 1,
                            'nav' => false,
                        ],
                        768 => [
                            'items' => 1,
                            'nav' => false,
                        ],
                    ],
                    'navClass' => [
                        'slider-prev',
                        'slider-next'
                    ],
                    'navElement' => 'div',
                    'navContainerClass' => 'owl-nav slider-nav',
                ],
                'clientScript' => new \yii\web\JsExpression("
//                        $('.play').on('click',function(){
//                            owl.trigger('play.owl.autoplay',[1000])
//                        });
//                        $('.stop').on('click',function(){
//                            owl.trigger('stop.owl.autoplay')
//                        });
//                        $(document).ready(function(){
//                            var owl = $('.owl-carousel');
//                            owl.on('mousewheel', '.owl-stage', function (e) {
//                                if (e.originalEvent.wheelDelta < 0) {
//                                    owl.trigger('next.owl');
//                                } else {
//                                    owl.trigger('prev.owl');
//                                }
//                                e.preventDefault();
//                            });
//                        });
                    "),
            ]);
            /** @var \common\models\Slider[] $slides */
            foreach ($slides as $key => $slide) { ?>
                <div class="item">
                    <a href="<?= !empty($slide->url) ? $slide->url : '#' ?>">
                        <h2><?= $slide->title ?></h2>
                        <img height="500" src="<?= $slide->image !== null ? $slide->image->href : '' ?>" alt="">
                    </a>
                </div>
            <?php } ?>
            <?php dominus77\owlcarousel2\WrapCarousel::end() ?>
        </div>
    </div>
    <div class="container">
        <div class="row text-center">
            <h2>ПРЕИМУЩЕСТВА ПОДГОТОВКИ</h2>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img width="100%" src="/images/icon-2.png" alt="">
                <br>
                <br>
                <label class="text-center">
                    Преподаватели - практикующие эксперты ЕГЭ
                </label>
            </div>
            <div class="col-md-4">
                <img width="100%" src="/images/icon-3.png" alt="">
                <br>
                <br>
                <label class="text-center">
                    Видеоролики<br> для быстрого и комфортного запоминания
                </label>
            </div>
            <div class="col-md-4">
                <img width="100%" src="/images/icon-4.png" alt="">
                <br>
                <br>
                <label class="text-center">
                    Возможность готовиться к ЕГЭ<br> из любой точки мира
                </label>
            </div>
        </div>
    </div>
    <div class="consultation-row">
        <div class="container">
            <?= \common\widgets\ContactFormWidget\ContactFormWidget::widget([
                'scenario' => 'consultation',
                'viewName' => 'consultation-inline',
                'theme' => 'Записывайся на консультацию',
                'send_to' => \common\models\Settings::getValue('consultation-e-mail')
            ]) ?>
        </div>
    </div>
    <div class="container">
        <div class="row text-center subject-block">
            <h2>ВЫБЕРИТЕ ПРЕДМЕТ ДЛЯ ПОДГОТОВКИ</h2>
            <?php /** @var \common\models\Subject $subjects */
            foreach ($subjects as $subject) {
                $image = $subject->image; ?>

                <div class="col-md-4 subject-item">
                    <div style="background-color: #D7E1F3;border-radius: 35px;min-height: 225px;">
                        <a href="<?= \yii\helpers\Url::toRoute(['course', 'subjectId' => $subject->id]) ?>">
                            <img width="100%" src="<?= $image !== null ? $image->rootPath : '' ?>" alt="">
                            <span class="subject-title"><?= $subject->title ?></span>
                        </a>
                    </div>
                </div>

            <?php } ?>
        </div>
    </div>
    <div class="ask-me-row">
        <div class="container">
            <h3>Задайте вопрос</h3>
            <?= \common\widgets\ContactFormWidget\ContactFormWidget::widget([
                'scenario' => \common\models\ContactForm::SCENARIO_DEFAULT,
                'viewName' => 'ask-me',
                'theme' => 'Задать вопрос',
                'send_to' => \common\models\Settings::getValue('faq-e-mail')
            ]) ?>
        </div>
    </div>
    <div class="text-center">
        <div class="container">
            <h2> ДОКУМЕНТЫ </h2>
            <div class="documents">
                <?php dominus77\owlcarousel2\WrapCarousel::begin([
                    'id' => 'docs-carousel',
                    'clientOptions' => [
                        'loop' => true,
                        'margin' => 10,
                        'nav' => false,
                        'dots' => true,
                        'autoplay' => false,
                        'autoplayTimeout' => 10000,
                        'autoplayHoverPause' => true,
                        'autoWidth' => false,
                        'responsive' => [
                            0 => [
                                'items' => 1,
                                'nav' => false,
                            ],
                            768 => [
                                'items' => 7,
                                'nav' => false,
                            ],
                        ],
                        'navClass' => [
                            'slider-prev',
                            'slider-next'
                        ],
                        'navElement' => 'div',
                        'navContainerClass' => 'owl-nav slider-nav',
                    ],
                ]); ?>
                <?php
                $documents = \common\models\Settings::getSiteDocuments();
                /** @var \floor12\files\models\File $document */
                foreach ($documents as $document) { ?>
                    <a class="fancybox" data-fancybox="gallery" rel="group" href="<?= $document->href ?>">
                        <img src="<?= $document->getPreviewWebPath(150, 200, true) ?>" alt=""/>
                    </a>
                <?php }
                ?>
                <?php dominus77\owlcarousel2\WrapCarousel::end() ?>
            </div>
        </div>
    </div>
    <div class="consultation-row">
        <div class="container">
            <div class="col-md-12 text-center">
                <h3> ОТЗЫВЫ </h3>
            </div>
            <?php
            $template = "{beginWrapper}\n{input}\n{endWrapper}";
            $model = new \common\models\Review();
            $form = ActiveForm::begin([
                'id' => 'kgsu-review-form',
                'options' => [
                    'data-role' => 'kgsu-review-form',
                ],
                'action' => \yii\helpers\Url::toRoute(['add-review']),
                'fieldConfig' => [
                    'template' => $template,
                ]
            ]);
            ?>
            <div class="col-md-8">
                <?= $form->field($model, 'fullname')->textInput(['placeholder' => 'Фамилия Имя Отчество'])->label(false) ?>
                <?= $form->field($model, 'content')->textarea(['placeholder' => 'Текст отзыва', 'rows' => 6])->label(false) ?>
            </div>
            <div class="col-md-4 text-center">
                <img width="200" src="/images/logo-white-1x1.png" alt="">
            </div>
            <div class="col-md-8">
                <div class="form-group text-right ">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-putc', 'name' => 'contact-button']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="container">
        <div class="row review-row">
            <?= \yii\widgets\ListView::widget([
                'dataProvider' => $reviewDataProvider,
                'itemOptions' => ['class' => 'item'],
                'summary' => false,
                'itemView' => function ($model, $key, $index, $widget) {
                    $out = '<div class="review"> <span class="text-right">';
                    $out .= $model->fullname;
                    $out .= '</span> <br> <br> <p>';
                    $out .= $model->content;
                    $out .= '</p> </div>';
                    return $out;
                },
                'pager' => [
                    'class' => \kop\y2sp\ScrollPager::className(),
                    'item' => '.item',
                    'triggerText' => 'ПОКАЗАТЬ ЕЩЕ',
                    'triggerTemplate' => '<div class="wrapper-show-more text-center ias-trigger" style="cursor: pointer"><a>{text}</a></div>',
                    'noneLeftText' => 'Записей больше нет',]
            ]) ?>
        </div>
    </div>
</div>