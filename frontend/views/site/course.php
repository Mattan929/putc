<?php

/* @var $this \yii\web\View */

use common\models\Subject;
use yii\helpers\Html;

/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\CourseSearch */
/* @var $searchParam [] */

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-form">
    <div class="container">
        <h3>
            Выберите нужный вам предмет.
        </h3>
        <?php $form = \yii\bootstrap\ActiveForm::begin([
                'method' => 'get'
        ])?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($searchModel, 'subject_ids')
                    ->checkboxList(\yii\helpers\ArrayHelper::map(Subject::findAll(['status' => Subject::STATUS_ACTIVE]), 'id', 'title'), [
                        'item'=>function ($index, $label, $name, $checked, $value) {
                            return '<div class="col-md-4">'.Html::checkbox($name,$checked,['label'=>$label,'value'=>$value]).'</div>';
                        }
                    ]) ?>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <?= Html::submitButton('Найти', ['class' => 'btn btn-putc'])?>
                <?= Html::a('Сбросить фильтр', ['site/course'], ['class' => 'btn btn-default'])?>
            </div>
        </div>
        <?php \yii\bootstrap\ActiveForm::end()?>
    </div>
</div>
<div class="container">
    <div class="site-course">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'summary' => false,
            'itemView' => function ($model, $key, $index, $widget) use ($searchParam) {
                $out = '';
                foreach ($model->subjects as $subject) {
                    if (!empty($searchParam['subject_ids'])){
                        if (in_array($subject->id, $searchParam['subject_ids'])){
                            $out .= $this->render('_course-view', ['model' => $model, 'subject' => $subject]);
                        }
                    } else {
                        $out .= $this->render('_course-view', ['model' => $model, 'subject' => $subject]);
                    }
                }
                return $out;
            },
            'pager' => [
                'class' => \kop\y2sp\ScrollPager::className(),
                'item' => '.item',
                'triggerText' => 'ПОКАЗАТЬ ЕЩЕ',
                'triggerTemplate' => '<div class="wrapper-show-more text-center ias-trigger" style="cursor: pointer"><a>{text}</a></div>',
                'noneLeftText' => '',]
        ])?>
    </div>
</div>
