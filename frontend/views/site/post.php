<?php

use common\models\Post;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model null|Post */

$this->title = Html::encode($model->title);
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['site/news']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-post">
    <?php if ($model->post_image !== null) { ?>
        <img class="pull-right" width="300" align="right" src="<?= $model->post_image ?>" alt="">
    <?php } ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= $model->content ?></p>

</div>
