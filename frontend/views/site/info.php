<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Информация по записи на курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="site-info">
        <h1><?= Html::encode($this->title) ?></h1>

        <p><?= \Faker\Provider\Lorem::text(2000)?></p>

    </div>
</div>
