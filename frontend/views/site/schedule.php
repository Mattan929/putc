<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;

/* @var $schedules \common\models\Schedule[] */

$this->title = 'Расписание';
$this->params['breadcrumbs'][] = $this->title;
$timeList = \common\models\ScheduleTime::getTimeList();
$dayList = \common\models\ScheduleRecord::getDayList();
?>
<div class="site-schedule">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach ($schedules as $schedule) {
        $course = $schedule->course;
        ?>
        <div class="row">
            <h2 class="text-center"><?= $course->title ?></h2>
            <?php
            $scheduleRecords = $schedule->getScheduleRecordsArray();
            for ($day = 1; $day < 7; $day++) {
                if ($day === 4) {
                    echo '<div class="clearfix"></div>';
                } ?>
                <div class="col-md-4">

                    <h5 class="text-center"><strong><?= $dayList[$day] ?></strong></h5>

                    <table class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th>Время</th>
                            <th>Предмет</th>
                            <th>Адрес - Аудитория</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($scheduleRecords[$day] as $scheduleRecord) {
                            $address = $scheduleRecord->address;
                            ?>
                            <tr>
                                <td><?= $timeList[$scheduleRecord->time_id] ?></td>
                                <td><?= $scheduleRecord->subject->title ?></td>
                                <td><?= $address !== null ? $address->address : '' ?>
                                    - <?= $scheduleRecord->classroom ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
