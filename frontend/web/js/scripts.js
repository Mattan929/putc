if (!mattan929 || typeof mattan929 === "undefined") {
    var mattan929 = {};
}

mattan929.mainJs = {

    init: function () {

        $(document).on('click', '[data-role=documents-modal-open]', function (e) {
            var modal = $(document).find($(this).data('target'));
            modal.data('id', $(this).data('id')).attr('data-id', $(this).data('id'));
            $.ajax({
                url: '/lk/documents-form',
                data: {id: $(this).data('id')},
                success: function (data) {
                    modal.find('.modal-body').html(data);
                    modal.modal('show');
                }
            });
        });

        $(document).on('click', '[data-role=save-documents-form]', function (e) {
            var modal = $(this).closest('.modal'),
                self = $(this),
                form = modal.find('#documents-form');

            $.ajax({
                method: 'POST',
                url: '/lk/documents-form?id=' + modal.data('id'),
                data: form.serialize(),
                success: function (data) {
                    if (data.success) {
                        modal.modal('hide');
                        mattan929.mainJs.alert(data.message, 'success');
                    }
                }
            });
        });

    },
    //Cообщение
    alert: function (message, status, timeout) {
        var uniqueId = '-' + Math.random().toString(36).substr(2, 9),
        timeout = timeout || 4500;
        var alert = '<div class="alert alert-' + status + ' alert-dismissable alert-fixed" data-alert="for-remove" data-role="alert-message' + uniqueId + '">';
        alert += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        alert += message;
        alert += '</div>';
        $('body').append(alert);
        mattan929.mainJs.alertTimeout = setTimeout(function () {
            $(document).find('[data-role=alert-message' + uniqueId + ']').hide();
            $(document).find('[data-role=alert-message' + uniqueId + ']').remove();
        }, timeout);
    }
};

mattan929.mainJs.init();
