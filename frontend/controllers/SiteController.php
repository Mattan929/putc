<?php
namespace frontend\controllers;

use common\models\Course;
use common\models\CourseSearch;
use common\models\CourseToUser;
use common\models\Employee;
use common\models\File;
use common\models\Post;
use common\models\Review;
use common\models\Schedule;
use common\models\Slider;
use common\models\Subject;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use http\Exception;
use Yii;
use yii\base\InvalidArgumentException;
use yii\data\ActiveDataProvider;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\ContactForm;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $subjects = Subject::findAll(['status' => Subject::STATUS_ACTIVE]);
        $reviewDataProvider = new ActiveDataProvider([
            'query' => Review::find()->where(['status' => Review::STATUS_PUBLISH]),
            'pagination' => [
                'pageSize' => 3
            ]
        ]);

        $slides = Slider::find()->where(['status' => Slider::STATUS_ACTIVE])->orderBy('order')->all();

        return $this->render('index', [
            'subjects' => $subjects,
            'reviewDataProvider' => $reviewDataProvider,
            'slides' => $slides
        ]);
    }

//    /**
//     * Logs in a user.
//     *
//     * @return mixed
//     */
//    public function actionLogin()
//    {
//        // Use this to interact with an API on the users behalf
//
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $this->render('index');
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        } else {
//            $model->password = '';
//
//            return $this->render('login', [
//                'model' => $model,
//            ]);
//        }
//    }

//    /**
//     * Logs out the current user.
//     *
//     * @return mixed
//     */
//    public function actionLogout()
//    {
//        Yii::$app->user->logout();
//
//        $provider = Yii::$app->azure;
//        $post_logout_redirect_uri = 'https://kgsu.loc/site/logout'; // The logout destination after the user is logged out from their account.
//        $logoutUrl = $provider->getLogoutUrl($post_logout_redirect_uri);
//        header('Location: '.$logoutUrl); // Redirect the user to the generated URL
//
////        return $this->goHome();
//    }

    /**
     * Displays contact page.
     *
     * @param $send_to
     * @return string
     */
    public function actionContact($send_to)
    {
        $data = Yii::$app->request->post('ContactForm', false);
        if ($data) {
            $model = new ContactForm();
            $model->scenario = $data['scenario'];
            if ($model->load(Yii::$app->request->post()) && $model->contact($send_to)) {
                switch ($model->scenario) {
                    case ContactForm::SCENARIO_CONTACT:
                        $message = '';
                        break;
                    case ContactForm::SCENARIO_APPLICATION:
                        $message = '';
                        break;
                    case ContactForm::SCENARIO_DEFAULT:
                        $message = 'Ваше вопрос отправлен. Ожидайте ответ на указанный вами адрес электронной почты.';
                        break;
                    case ContactForm::SCENARIO_CONSULTATION:
                        $message = 'Ваша заявка принята. В течении рабочего дня Вам перезвонят.';
                        break;
                    default: $message = 'Ваше действие имело успех.';

                }
                Yii::$app->session->setFlash('success', $message);

                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->redirect(['page/index', 'slug' => 'o-nas']);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionTeacher()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Employee::find()->where(['status' => Employee::STATUS_ACTIVE]),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('teacher', ['dataProvider' => $dataProvider]);
    }

//    /**
//     * Signs user up.
//     *
//     * @return mixed
//     */
//    public function actionSignup()
//    {
//        $model = new SignupForm();
//        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
//            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
//            return $this->goHome();
//        }
//
//        return $this->render('signup', [
//            'model' => $model,
//        ]);
//    }

//    /**
//     * Requests password reset.
//     *
//     * @return mixed
//     */
//    public function actionRequestPasswordReset()
//    {
//        $model = new PasswordResetRequestForm();
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if ($model->sendEmail()) {
//                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
//
//                return $this->goHome();
//            } else {
//                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
//            }
//        }
//
//        return $this->render('requestPasswordResetToken', [
//            'model' => $model,
//        ]);
//    }

//    /**
//     * Resets password.
//     *
//     * @param string $token
//     * @return mixed
//     * @throws BadRequestHttpException
//     */
//    public function actionResetPassword($token)
//    {
//        try {
//            $model = new ResetPasswordForm($token);
//        } catch (InvalidArgumentException $e) {
//            throw new BadRequestHttpException($e->getMessage());
//        }
//
//        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
//            Yii::$app->session->setFlash('success', 'New password saved.');
//
//            return $this->goHome();
//        }
//
//        return $this->render('resetPassword', [
//            'model' => $model,
//        ]);
//    }

//    /**
//     * Verify email address
//     *
//     * @param string $token
//     * @throws BadRequestHttpException
//     * @return yii\web\Response
//     */
//    public function actionVerifyEmail($token)
//    {
//        try {
//            $model = new VerifyEmailForm($token);
//        } catch (InvalidArgumentException $e) {
//            throw new BadRequestHttpException($e->getMessage());
//        }
//        if ($user = $model->verifyEmail()) {
//            if (Yii::$app->user->login($user)) {
//                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
//                return $this->goHome();
//            }
//        }
//
//        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
//        return $this->goHome();
//    }

//    /**
//     * Resend verification email
//     *
//     * @return mixed
//     */
//    public function actionResendVerificationEmail()
//    {
//        $model = new ResendVerificationEmailForm();
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if ($model->sendEmail()) {
//                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
//                return $this->goHome();
//            }
//            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
//        }
//
//        return $this->render('resendVerificationEmail', [
//            'model' => $model
//        ]);
//    }

    public function actionAddReview()
    {
        $model = new Review();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Отзыв отправлен. Спасибо за Ваш отзыв.');

            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionNews()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Post::find()->where(['status' => Post::STATUS_PUBLISH])->orderBy(['id' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
        return $this->render('news', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionInfo()
    {
        return $this->render('info');
    }

    public function actionSchedule()
    {
        $schedules = Schedule::findAll(['status' => Schedule::STATUS_ACTIVE]);

        return $this->render('schedule', [
            'schedules' => $schedules
        ]);
    }

    public function actionPost($id)
    {
        $model = Post::findOne($id);

        return $this->render('post', ['model' => $model]);
    }

    public function actionCourse($subjectId = null)
    {
        $searchModel = new CourseSearch();
        if ($subjectId !== null){
            $searchModel->subject_ids = [$subjectId];
            $dataProvider = $searchModel->search($searchModel);
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->get());
        }
        $searchParam = Yii::$app->request->get('CourseSearch', false);

        return $this->render('course', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'searchParam' => $searchParam,
        ]);
    }
}
