<?php

namespace frontend\controllers;

use common\models\Page;
use yii\web\NotFoundHttpException;

class PageController extends \yii\web\Controller
{
    public function actionIndex($slug)
    {
        $model = $this->findModel($slug);

        return $this->render('index', ['model' => $model]);
    }


    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param $slug
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     * @internal param int $id
     */
    protected function findModel($slug)
    {
        if (($model = Page::findOne(['slug' => $slug, 'status' => Page::STATUS_PUBLISH])) !== null) {
            return $model;
        }


        throw new NotFoundHttpException('Запрашиваемая страница не существует.');
    }

}
