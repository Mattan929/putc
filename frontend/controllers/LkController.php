<?php

namespace frontend\controllers;

use common\models\CourseToUser;
use common\models\Settings;
use Yii;
use yii\bootstrap\BootstrapAsset;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;

class LkController extends \yii\web\Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [

                    [
                        'actions' => [
                            'info',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'index',
                            'my-course',
                            'enroll-to-course',
                            'refuse',
                            'info',
                            'documents-form'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $this->layout = 'lk-main';
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionMyCourse()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CourseToUser::find()->where([
                'AND',
                ['user_id' => Yii::$app->user->identity->id],
                ['>', 'status', CourseToUser::STATUS_REFUSED]])
        ]);

        return $this->render('my-course', ['dataProvider' => $dataProvider]);
    }


    public function actionRefuse($id)
    {
        if ($model = CourseToUser::findOne($id)){
            $model->status = CourseToUser::STATUS_REFUSED;

            if ($model->save()) {
                return $this->redirect('/lk/my-course');
            }
            Yii::$app->session->setFlash('danger', 'Ошибка. Перезагрузите страницу и попробуйте снова.');
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->session->setFlash('danger', 'Ошибка. Курс не найден.');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionEnrollToCourse($courseId, $subjectId)
    {
        $model = new CourseToUser();

        $model->course_id = $courseId;
        $model->subject_id = $subjectId;

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Запись прошла успешно. Дальнейшие инструкции отправлены на адрес электронной почты, указанный при регистрации.');
            return $this->redirect('/lk/my-course');
        }
        Yii::$app->session->setFlash('danger', 'Ошибка. Перезагрузите страницу и попробуйте снова.');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionInfo()
    {
        $this->view->title = 'Информация по записи';
        $this->view->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['/lk']];
        $this->view->params['breadcrumbs'][] = $this->view->title;
        return $this->render('info');
    }

    public function actionDocumentsForm($id)
    {
        $model = CourseToUser::findOne($id);
        $user = Yii::$app->user->identity;

        if ($model && $model->load(Yii::$app->request->post())) {
            if (!$model->save()) {
                var_dump($model->getErrors());
                die;
            }
            if ($user->load(Yii::$app->request->post())) {
                if (!$user->save()){
                    var_dump($model->getErrors());
                    die;
                }
            }
//            Yii::$app->session->setFlash('success', 'Данные сохранены.');
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true, 'message' => 'Данные сохранены.'];
        }
        Yii::$app->assetManager->bundles = [
            'yii\web\JqueryAsset' => false,
            BootstrapAsset::class => false
        ];
        $documents = Settings::findOne(['code' => 'documents']);

        return $this->renderAjax('docs', ['model' => $model, 'documents' => $documents]);
    }

}
