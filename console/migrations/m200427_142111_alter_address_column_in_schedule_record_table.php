<?php

use yii\db\Migration;

/**
 * Class m200427_142111_alter_address_column_in_shedule_record_table
 */
class m200427_142111_alter_address_column_in_schedule_record_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('schedule_record', 'address_id', $this->integer());
        $this->dropColumn('schedule_record', 'address', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('schedule_record', 'address_id', $this->integer());
        $this->addColumn('schedule_record', 'address', $this->string());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200427_142111_alter_address_column_in_shedule_record_table cannot be reverted.\n";

        return false;
    }
    */
}
