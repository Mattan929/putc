<?php

use yii\db\Migration;

/**
 * Class m201101_054520_add_column_moodle_category_id_to_course_table
 */
class m201101_054520_add_column_moodle_category_id_to_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('course', 'moodle_category_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('course', 'moodle_category_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201101_054520_add_column_moodle_category_id_to_course_table cannot be reverted.\n";

        return false;
    }
    */
}
