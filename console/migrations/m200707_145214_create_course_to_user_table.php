<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course_to_user}}`.
 */
class m200707_145214_create_course_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course_to_user}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'subject_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->addForeignKey('fk-course_to_user-user_id', 'course_to_user', 'user_id', 'user', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk-course_to_user-course_id', 'course_to_user', 'course_id', 'course', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk-course_to_user-subject_id', 'course_to_user', 'subject_id', 'subject', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%course_to_user}}');
    }
}
