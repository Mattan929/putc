<?php

use yii\db\Migration;

/**
 * Class m200517_170353_add_record_to_settings_table
 */
class m200517_170353_add_record_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            'title' => 'Документы в раздел "Документы" на главной',
            'value' => 'documents',
            'additional_value' => '',
            'cant_be_removed' => 1,
            'code' => 'documents',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200517_170353_add_record_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200517_170353_add_record_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
