<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%review}}`.
 */
class m200425_052332_create_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'fullname' => $this->string()->notNull(),
            'content' => $this->text(5000)->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%review}}');
    }
}
