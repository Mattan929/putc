<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m200516_204043_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'value' => $this->text()->notNull(),
            'additional_value' => $this->text()->notNull(),
            'cant_be_removed' => $this->smallInteger(1)->defaultValue(0),
            'code' => $this->string()
        ]);

        $this->insert('{{%settings}}', [
            'title' => 'Электронная почта для консультаций',
            'value' => 'mai@kdsu.ru',
            'additional_value' => '',
            'cant_be_removed' => 1,
            'code' => 'consultation-e-mail',
        ]);

        $this->insert('{{%settings}}', [
            'title' => 'Электронная почта для "Задать вопрос"',
            'value' => 'mai@kdsu.ru',
            'additional_value' => '',
            'cant_be_removed' => 1,
            'code' => 'faq-e-mail',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
