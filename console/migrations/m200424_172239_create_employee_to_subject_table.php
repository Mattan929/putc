<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee_to_subject}}`.
 */
class m200424_172239_create_employee_to_subject_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee_to_subject}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer(),
            'subject_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee_to_subject}}');
    }
}
