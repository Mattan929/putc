<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%schedule_time}}`.
 */
class m200427_143234_create_schedule_time_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%schedule_time}}', [
            'id' => $this->primaryKey(),
            'time' => $this->string()->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%schedule_time}}');
    }
}
