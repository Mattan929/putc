<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course_to_subject}}`.
 */
class m200421_171600_create_course_to_subject_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course_to_subject}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(),
            'subject_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%course_to_subject}}');
    }
}
