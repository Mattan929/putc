<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%schedule_record}}`.
 */
class m200425_122453_create_schedule_record_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%schedule_record}}', [
            'id' => $this->primaryKey(),
            'day_id' => $this->smallInteger(1)->notNull(),
            'time_id' => $this->smallInteger(2)->notNull(),
            'schedule_id' => $this->integer()->notNull(),
            'subject_id' => $this->integer()->notNull(),
            'address' => $this->string(),
            'classroom' => $this->string(),
        ]);

        $this->addForeignKey('fk-schedule_record-schedule_id', 'schedule_record', 'schedule_id', 'schedule', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk-schedule_record-subject_id', 'schedule_record', 'subject_id', 'subject', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-schedule_record-schedule_id', 'schedule_record');
        $this->dropForeignKey('fk-schedule_record-subject_id', 'schedule_record');

        $this->dropTable('{{%schedule_record}}');
    }
}
