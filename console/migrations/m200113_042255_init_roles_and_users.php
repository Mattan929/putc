<?php

use yii\db\Migration;

/**
 * Class m200113_042255_first_init
 */
class m200113_042255_init_roles_and_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // создание ролей
        $this->insert('{{%auth_item}}', [
            'name' => 'superadmin',
            'type' => 1,
            'description' => 'Superadmin',
            'rule_name' => NULL,
            'data' => time(),
            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'moderator',
            'type' => 1,
            'description' => 'Модератор',
            'created_at' => time(),
            'updated_at' => time()
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'teacher',
            'type' => 1,
            'description' => 'Преподаватель',
            'created_at' => time(),
            'updated_at' => time()
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'student',
            'type' => 1,
            'description' => 'Студент',
            'created_at' => time(),
            'updated_at' => time()
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'enrollee',
            'type' => 1,
            'description' => 'Абитуриент',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'superadmin',
            'child' => 'moderator'
        ]);

        // инит суперадмина
        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'administrator',
            'email' => 'administrator@localhost.lc',
            'password_hash' => '$2y$13$b94CY5bXZyA.VnJrD8KWtuxNQqJiUZ/X7D7rSAHuvCDju7L6VsnM.',
            'auth_key' => 'Ijw2fHuPrgqh9gbTtmYMpuXUzmiH51Tn',
            'created_at' => time(),
            'confirmed_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%auth_assignment}}', [
            'item_name' => 'superadmin',
            'user_id' => 1,
            'created_at' => time(),
        ]);

        // инит модератора
        $this->insert('user', [
            'id' => 2,
            'username' => 'moderator',
            'email' => 'moderator@moderator.moderator',
            'password_hash' => '$2y$10$xjfbEPOTmH7gql/qigUWxOH0mYiz/TDWwYUkm2IyjeCXnJBmw3Y8i',
            'auth_key' => 'lT7IuI_XsQitesl20IkbEmnFJxR5F51E',
            'confirmed_at' => time(),
            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $this->insert('{{%auth_assignment}}', [
            'item_name' => 'moderator',
            'user_id' => 2,
            'created_at' => time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS = 0")->execute();
        $this->truncateTable('auth_item');
        $this->truncateTable('auth_item_child');
        $this->truncateTable('auth_assignment');
        \Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS = 1")->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200113_042255_first_init cannot be reverted.\n";

        return false;
    }
    */
}
