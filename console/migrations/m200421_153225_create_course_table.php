<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course}}`.
 */
class m200421_153225_create_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'subject_id' => $this->integer(),
            'status' => $this->smallInteger(1)->defaultValue(\common\models\Course::STATUS_DRAFT),
            'total_hours' => $this->integer(),
            'hours_in_month' => $this->integer(),
            'hours_in_week' => $this->integer(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'begin_date' => $this->integer(11),
            'end_date' => $this->integer(11),
            'cost' => $this->float(),
        ]);

        $this->createIndex('idx-course-created_at', 'course', 'created_at');
        $this->createIndex('idx-course-begin_date', 'course', 'begin_date');
        $this->createIndex('idx-course-end_date', 'course', 'end_date');
        $this->createIndex('idx-course-begin_date-end_date', 'course', ['end_date', 'begin_date']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-course-created_at', 'course');
        $this->dropIndex('idx-course-begin_date', 'course');
        $this->dropIndex('idx-course-end_date', 'course');
        $this->dropIndex('idx-course-begin_date-end_date', 'course');

        $this->dropTable('{{%course}}');
    }
}
