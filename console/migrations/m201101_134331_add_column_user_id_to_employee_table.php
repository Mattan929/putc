<?php

use yii\db\Migration;

/**
 * Class m201101_134331_add_column_user_id_to_employee_table
 */
class m201101_134331_add_column_user_id_to_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('employee', 'user_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('employee', 'user_id');
    }
}
