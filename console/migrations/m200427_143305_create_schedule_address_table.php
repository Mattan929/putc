<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%schedule_address}}`.
 */
class m200427_143305_create_schedule_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%schedule_address}}', [
            'id' => $this->primaryKey(),
            'address' => $this->string()->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%schedule_address}}');
    }
}
