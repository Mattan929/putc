<?php

use yii\db\Migration;

/**
 * Class m201101_052948_insert_moodle_record_settings_table
 */
class m201101_052948_insert_moodle_record_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            'title' => 'ID главной категории в moodle',
            'value' => '94',
            'additional_value' => '',
            'cant_be_removed' => 1,
            'code' => 'main-moodle-category',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201101_052948_insert_moodle_record_settings_table cannot be reverted.\n";

        return false;
    }
}
