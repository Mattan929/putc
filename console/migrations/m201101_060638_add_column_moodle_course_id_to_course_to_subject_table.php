<?php

use yii\db\Migration;

/**
 * Class m201101_060638_add_column_moodle_course_id_to_course_to_subject_table
 */
class m201101_060638_add_column_moodle_course_id_to_course_to_subject_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('course_to_subject', 'moodle_course_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('course_to_subject', 'moodle_course_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201101_060638_add_column_moodle_course_id_to_course_to_subject_table cannot be reverted.\n";

        return false;
    }
    */
}
