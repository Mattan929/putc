<?php

use yii\db\Migration;

/**
 * Class m200421_103557_add_status_column_in_user_table
 */
class m200421_103557_add_status_column_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'status', $this->smallInteger(2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200421_103557_add_status_column_in_user_table cannot be reverted.\n";

        return false;
    }
    */
}
