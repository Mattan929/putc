<?php

namespace backend\controllers;

use common\models\ScheduleTime;
use common\models\search\ScheduleTimeSearch;
use common\models\search\SettingsSearch;
use common\models\Settings;
use Yii;
use common\models\ScheduleAddress;
use common\models\search\ScheduleAddressSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SettingsController implements the CRUD actions for ScheduleAddress model.
 */
class SettingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete-address',
                            'delete-time',
                            'delete'
                        ],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'create-address',
                            'update',
                            'update-address',
                            'create-time',
                            'update-time',
                            'index',
                            'enable-time',
                            'enable-address',
                            'disable-time',
                            'disable-address',
                        ],
                        'allow' => true,
                        'roles' => ['moderator'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all ScheduleAddress models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModelAddress = new ScheduleAddressSearch();
        $dataProviderAddress = $searchModelAddress->search(Yii::$app->request->queryParams);
        $searchModelTime = new ScheduleTimeSearch();
        $dataProviderTime  = $searchModelTime ->search(Yii::$app->request->queryParams);
        $searchModel = new SettingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelTime' => $searchModelTime,
            'dataProviderTime' => $dataProviderTime,
            'searchModelAddress' => $searchModelAddress,
            'dataProviderAddress' => $dataProviderAddress,
        ]);
    }

    /**
     * Creates a new Settings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Settings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Settings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Settings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Settings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Displays a single ScheduleAddress model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('time/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScheduleAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateAddress()
    {
        $model = new ScheduleAddress();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->redirect('index');
    }

    /**
     * Creates a new ScheduleAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTime()
    {
        $model = new ScheduleTime();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->redirect('index');
    }

    /**
     * Updates an existing ScheduleAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateAddress($id)
    {
        $model = $this->findModelAddress($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('address/update', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ScheduleAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateTime($id)
    {
        $model = $this->findModelTime($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('time/update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ScheduleAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteTime($id)
    {
        $this->findModelTime($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing ScheduleAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteAddress($id)
    {
        $this->findModelAddress($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScheduleAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScheduleAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelAddress($id)
    {
        if (($model = ScheduleAddress::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the ScheduleAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScheduleTime the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelTime($id)
    {
        if (($model = ScheduleTime::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDisableTime($id)
    {
        if(!$this->findModelTime($id)->disable()){
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Время отключено');

        return $this->redirect(['index']);
    }

    public function actionEnableTime($id)
    {
        if(!$this->findModelTime($id)->enable()){
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Время включено');
        return $this->redirect(['index']);
    }

    public function actionDisableAddress($id)
    {
        if(!$this->findModelAddress($id)->disable()){
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Адрес отключен');

        return $this->redirect(['index']);
    }

    public function actionEnableAddress($id)
    {
        if(!$this->findModelAddress($id)->enable()){
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Адрес включен');
        return $this->redirect(['index']);
    }
}
