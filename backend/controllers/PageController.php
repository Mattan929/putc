<?php

namespace backend\controllers;

use floor12\files\logic\FileCreateFromInstance;
use Yii;
use common\models\Page;
use common\models\search\PageSearch;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'publish',
                            'restore',
                            'hide',
                            'upload-img',
                        ],
                        'allow' => true,
                        'roles' => ['moderator'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPublish($id)
    {
        $model = $this->findModel($id);
        if (!$model->publish()){
            var_dump($model->getErrors());
        }
        return $this->redirect(['index']);
    }

    public function actionHide($id)
    {
        $model = $this->findModel($id);
        if (!$model->hide()){
            var_dump($model->getErrors());
        }
        return $this->redirect(['index']);
    }

    public function actionRestore($id)
    {
        $this->findModel($id)->publish();
        return $this->redirect(['index']);
    }

    public function actionUploadImg($CKEditorFuncNum)
    {
        $file = UploadedFile::getInstanceByName('upload');
        if ($file){
            $data = array_merge(
                [
                    'modelClass' => 'common\\models\\Page',
                    'attribute' => 'files',
                    'mode' => 'multi',
                    'ratio' => 0
                ],
                Yii::$app->request->post()
            );

            $model = Yii::createObject(FileCreateFromInstance::class, [
                $file,
                $data,
                Yii::$app->user->identity,
            ])->execute();


            if ($model->errors) {
                throw new BadRequestHttpException('Ошибки валидации модели');
            }

            if ($model->save()){
                $ratio = Yii::$app->request->post('ratio', null);

                $view = Yii::$app->request->post('mode') === 'single' ? "_single" : "_file";

                $result = $this->renderAjax('@floor12/files/views/default/' . $view, [
                    'model' => $model,
                    'ratio' => $ratio
                ]);

                return '<script type="text/javascript">
                            window.parent.CKEDITOR.tools.callFunction("'.$CKEditorFuncNum.'", "' . $model->href.'", "");
                       </script>';

            } else
                return "Возникла ошибка при загрузке файла\n";
        } else
            return "Файл не загружен\n";
    }
}
