<?php

namespace backend\controllers;

use common\models\CourseToSubject;
use common\models\CourseToUser;
use common\models\User;
use common\models\UserToMoodleCourse;
use dektrium\user\models\RegistrationForm;
use Yii;
use common\models\Employee;
use common\models\search\EmployeeSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'enable',
                            'disable',
                            'create-user',
                            'course',
                        ],
                        'allow' => true,
                        'roles' => ['moderator'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Employee();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateUser($id)
    {
        $model = new RegistrationForm();
        $emploee = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            $user = User::findOne(['username' => $model->username]);
            if ($user) {
                $user->confirm();
                $auth = Yii::$app->authManager;
                $role = $auth->getRole('teacher');
                $auth->assign($role, $user->id);
                $emploee->user_id = $user->id;
                $emploee->save();
            }

            return $this->redirect(['index']);
        }

        return $this->render('create-user', [
            'model' => $model,
            'emploee' => $emploee,
        ]);
    }

    public function actionCourse($id)
    {
        $emploee = $this->findModel($id);
        /** @var User $user */
        $user = $emploee->user;

        if (Yii::$app->request->post()) {
            $items = Yii::$app->request->post('MoodleCourseIds', []);
            $courseToSubjectItems = CourseToSubject::findAll(['id' => $items]);
            $userToCourse = CourseToUser::findAll(['user_id' => $user->id]);
            $different = [];
            $courseToSubjects = [];
            foreach ($userToCourse as $record) {
                $courseToSubject = $record->courseToSubject;
                if (!ArrayHelper::isIn($courseToSubject, $courseToSubjectItems)){
                    $different[] = $courseToSubject;
                }
            }
            if (is_array($different) && count($different) > 0){
                $success = true;
                foreach ($different as $course) {
                    /** @var CourseToSubject $course */
                    $success = $user->unenrol($course->moodle_course_id);
                    if ($success && $courseToUser = $course->courseToUser) {
                        $courseToUser->delete();
                    }
                }
            }
            CourseToUser::deleteAll(['user_id' => $user->id]);
            foreach ($courseToSubjectItems as $courseToSubjectItem) {
                $success = $user->enrol($courseToSubjectItem->moodle_course_id, User::MOODLE_ROLE_EDITINGTEACHER);
                $model = new CourseToUser();
                $model->detachBehavior('blameable');
                $model->user_id = $user->id;
                $model->course_id = $courseToSubjectItem->course_id;
                $model->subject_id = $courseToSubjectItem->subject_id;
                $model->status = CourseToUser::STATUS_PAYMENT;
                $model->save();
            }
        }

        return $this->render('course', [
            'emploee' => $emploee,
        ]);
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
