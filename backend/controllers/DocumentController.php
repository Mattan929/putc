<?php

namespace backend\controllers;

use floor12\files\models\File;
use yii\data\ActiveDataProvider;

class DocumentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = File::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

}
