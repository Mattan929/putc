<?php

namespace backend\controllers;

use Yii;
use common\models\Review;
use common\models\search\ReviewSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'publish',
                            'hide',
                        ],
                        'allow' => true,
                        'roles' => ['moderator'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Review models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Review model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Review model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Review();

        if ($model->load(Yii::$app->request->post())) {
            $model->setCreatedDate(Yii::$app->request->post('Review')['createdDate']);
            if (empty(Yii::$app->request->post('Review')['updatedDate'])) {
                $model->setUpdatedDate(Yii::$app->request->post('Review')['createdDate']);
            } else {
                $model->setUpdatedDate(Yii::$app->request->post('Review')['updatedDate']);
            }
            if (!$model->save()) {
                var_dump($model->getErrors());
                die;
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->setCreatedDate(Yii::$app->request->post('Review')['createdDate']);
            $model->setUpdatedDate(Yii::$app->request->post('Review')['updatedDate']);
            if (!$model->save()) {
                var_dump($model->getErrors());
                die;
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionHide($id)
    {
        if(!$this->findModel($id)->hide()){
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Отзыв скрыт');

        return $this->redirect(['index']);
    }

    public function actionPublish($id)
    {
        if(!$this->findModel($id)->publish()){
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Отзыв опубликован');
        return $this->redirect(['index']);
    }
}
