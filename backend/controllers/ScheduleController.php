<?php

namespace backend\controllers;

use common\models\ScheduleRecord;
use Yii;
use common\models\Schedule;
use common\models\search\ScheduleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ScheduleController implements the CRUD actions for Schedule model.
 */
class ScheduleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'enable',
                            'disable',
                            'add-new-schedule-record',
                        ],
                        'allow' => true,
                        'roles' => ['moderator'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Schedule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScheduleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Schedule model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Schedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schedule();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Schedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $scheduleRecords = Yii::$app->request->post('ScheduleRecords');
            ScheduleRecord::deleteAll([
                'schedule_id' => $model->id,
            ]);
            foreach ($scheduleRecords as $dayRecords) {
                foreach ($dayRecords as $dayRecord) {
                    $scheduleRecord = new ScheduleRecord();
                    $scheduleRecord->attributes = $dayRecord;

                    if (!$scheduleRecord->validate()){
                        continue;
                    }
                    if (!$scheduleRecord->save()) {
                        return var_dump($scheduleRecord->getErrors());
                    }
                }
            }

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Schedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schedule::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDisable($id)
    {
        if (!$this->findModel($id)->disable()) {
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Расписание отключено');

        return $this->redirect(['index']);
    }

    public function actionEnable($id)
    {
        if (!$this->findModel($id)->enable()) {
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Расписание включено');
        return $this->redirect(['index']);
    }

    public function actionAddNewScheduleRecord($scheduleId, $dayId, $nextKey)
    {
        $model = $this->findModel($scheduleId);

        return $this->renderAjax('_day-element', [
            'element' => new ScheduleRecord(),
            'day_id' => $dayId,
            'schedule_id' => $model->id,
            'course' => $model->course,
            'key' => $nextKey
        ]);
    }
}
