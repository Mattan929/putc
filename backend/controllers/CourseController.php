<?php

namespace backend\controllers;

use common\models\CourseToSubject;
use common\models\MoodleCourse;
use common\models\Subject;
use Yii;
use common\models\Course;
use common\models\CourseSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'index',
                            'publish',
                            'unpublish',
                            'import'
                        ],
                        'allow' => true,
                        'roles' => ['moderator'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();

        if ($model->load(Yii::$app->request->post())) {
            $model->setBeginDate(Yii::$app->request->post('Course')['beginDate']);
            $model->setEndDate(Yii::$app->request->post('Course')['endDate']);
            if (!$model->save()) {
                var_dump($model->getErrors());
                die;
            }
            $categoryId = $model->addMoodleCategory();
            $model->setCategory($categoryId);

            if ($subjects = $model->subjects) {
                /** @var Subject $subject */
                foreach ($subjects as $subject) {
                    $modelM = new MoodleCourse();
                    $modelM->scenario = MoodleCourse::SCENARIO_CREATE;
                    $modelM->fullname = $subject->title;
                    $modelM->shortname = $model->title . ' (' . $subject->short_title . ')';
                    $modelM->categoryid = $categoryId;
                    $data = $modelM->addMoodleCourse();
                    if (isset($data['exception'])) {
                        var_dump($data['message']);
                        die;
                    }
                    /** @var CourseToSubject $subjectToCourse */
                    $subjectToCourse = CourseToSubject::findOne(['subject_id' => $subject->id, 'course_id' => $model->id]);
                    if ($subjectToCourse) {
                        $subjectToCourse->moodle_course_id = $data[0]['id'];
                        if (!$subjectToCourse->save()) {
                            var_dump($data[0]['id']);
                            var_dump($subjectToCourse->getErrors());
                            die;
                        }
                    }
                }
            }

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->setBeginDate(Yii::$app->request->post('Course')['beginDate']);
            $model->setEndDate(Yii::$app->request->post('Course')['endDate']);
            if (!$model->save()) {
                var_dump($model->getErrors());
                die;
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionUnpublish($id)
    {
        if (!$this->findModel($id)->unpublish()) {
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Курс скрыт');

        return $this->redirect(['index']);
    }

    public function actionPublish($id)
    {
        if (!$this->findModel($id)->publish()) {
            Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже');
        }
        Yii::$app->session->setFlash('success', 'Курс опубликован');
        return $this->redirect(['index']);
    }

    public function actionImport($id)
    {
        $course = $this->findModel($id);

        $categoryId = $course->addMoodleCategory();
        $course->setCategory($categoryId);
        $subjects = $course->subjects;
        /** @var Subject $subject */
        foreach ($subjects as $subject){
            $model = new MoodleCourse();
            $model->scenario = MoodleCourse::SCENARIO_CREATE;
            $model->fullname = $subject->title;
            $model->shortname = $course->title . ' (' . $subject->short_title . ')';
            $model->categoryid = $categoryId;
            $data = $model->addMoodleCourse();
            if (isset($data['exception'])){
                var_dump($data['message']);
                die;
            }
            /** @var CourseToSubject $subjectToCourse */
            $subjectToCourse = CourseToSubject::findOne(['subject_id' => $subject->id, 'course_id' => $id]);
            if ($subjectToCourse) {
                $subjectToCourse->moodle_course_id = $data[0]['id'];
                if(!$subjectToCourse->save()){
                    var_dump($data[0]['id']);
                    var_dump($subjectToCourse->getErrors());
                    die;
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
