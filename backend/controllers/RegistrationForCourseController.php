<?php

namespace backend\controllers;

use common\models\Course;
use common\models\CourseToSubject;
use common\models\User;
use Yii;
use common\models\CourseToUser;
use common\models\search\CourseToUserSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegistrationForCourseController implements the CRUD actions for CourseToUser model.
 */
class RegistrationForCourseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CourseToUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseToUserSearch();
        $userIds = \Yii::$app->authManager->getUserIdsByRole('teacher');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['NOT IN', 'user_id', $userIds]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CourseToUser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CourseToUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CourseToUser();

        $model->detachBehavior('blameable');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CourseToUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CourseToUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CourseToUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseToUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseToUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionRefuse($id)
    {
        if ($model = $this->findModel($id)){
            $model->status = CourseToUser::STATUS_REFUSED;

            if ($model->save()) {
                return $this->redirect('index');
            }
            Yii::$app->session->setFlash('danger', 'Ошибка. Перезагрузите страницу и попробуйте снова.');
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->session->setFlash('danger', 'Ошибка. Курс не найден.');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApprove($id)
    {
        if ($model = $this->findModel($id)){
            $model->status = CourseToUser::STATUS_APPROVED;

            if ($model->save()) {
                return $this->redirect('index');
            }
            Yii::$app->session->setFlash('danger', 'Ошибка. Перезагрузите страницу и попробуйте снова.');
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->session->setFlash('danger', 'Ошибка. Курс не найден.');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionOpenAccess($id)
    {
        if ($model = $this->findModel($id)){
            $model->status = CourseToUser::STATUS_PAYMENT;
            $user = $model->user;

            if ($model->save()) {
                if ($courseToSubjectItem = $model->courseToSubject) {
                    /** @var CourseToSubject $courseToSubjectItem */
                    $success = $user->enrol($courseToSubjectItem->moodle_course_id, User::MOODLE_ROLE_STUDENT);
                }
                return $this->redirect('index');
            }
            Yii::$app->session->setFlash('danger', 'Ошибка. Перезагрузите страницу и попробуйте снова.');
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->session->setFlash('danger', 'Ошибка. Курс не найден.');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionGetSubject() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $course = Course::findOne($cat_id);

                $out = $course->getSubjects()->select(['id', 'title AS name'])->asArray()->all();
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }
}
