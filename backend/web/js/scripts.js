if (!mattan929 || typeof mattan929 === "undefined") {
    var mattan929 = {};
}

mattan929.adminFunctions = {

    init: function () {

        $(document).on('click', '[data-role=add-new-schedule-record]', function (e) {
            var settings = $(this).closest('[data-role=settings]').data(),
                self = $(this);

            $.ajax({
                url: settings.addUrl,
                data: settings,
                success: function (data) {
                    self.closest('[data-role=schedule-record-block]').after(data);
                    self.closest('[data-role=settings]').data('next-key', settings.nextKey + 1).attr('data-next-key', settings.nextKey + 1);
                }
            })

        });

        $(document).on('click', '[data-role=remove-schedule-record]', function (e) {
            $(this).closest('[data-role=schedule-record-block]').remove();
        });

    },
};

mattan929.adminFunctions.init();
