<?php
/* @var $this yii\web\View */

use floor12\files\components\FileInputWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Документы';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="settings-form">

    <?php $form = ActiveForm::begin([
            'action' => \yii\helpers\Url::to(['documents/update', 'id' => $model->id])
    ]); ?>

    <?= $form->field($model, 'documents')->widget(FileInputWidget::class)->label(false) ?>
    <?= $form->field($model, 'application')->widget(FileInputWidget::class)->label() ?>
    <?= $form->field($model, 'confirmation')->widget(FileInputWidget::class)->label() ?>
    <?= $form->field($model, 'contract_offer')->widget(FileInputWidget::class)->label() ?>

    <?= $form->field($model, 'title')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'value')->hiddenInput()->label(false)  ?>

    <?= $form->field($model, 'additional_value')->hiddenInput()->label(false)  ?>

    <?php if (Yii::$app->user->can('superadmin')) {
        echo $form->field($model, 'cant_be_removed')->hiddenInput()->label(false) ;
    } ?>

    <?= $form->field($model, 'code')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>