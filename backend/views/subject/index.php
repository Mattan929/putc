<?php

use common\models\Subject;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SubjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Предметы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-index">

    <p>
        <?= Html::a('Добавить предмет', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'short_title',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status', Subject::getStatusList(), ['class'=>'form-control', 'prompt' => 'Все']),
                'value' => function ($model) {
                    if ($model->status === Subject::STATUS_ACTIVE){
                        return Html::tag('span', 'ВКЛ', [
                            'class' => 'label label-success',
                        ]);
                    }
                    return Html::tag('span', 'ВЫКЛ', [
                        'class' => 'label label-danger',
                    ]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{action}{update}{delete}',
                'buttons' => [
                    'action' => function($url, $model, $key){
                        if ($model->status === Subject::STATUS_ACTIVE){
                            return Html::a('Отключить', ['disable', 'id' => $model->id], [
                                'class' => 'label label-danger',
                                'title' => 'Нажмите, чтобы включить.',
                                'data-confirm' => 'Вы действительно хотите отключить данный элемент?'
                            ]);
                        }
                        return Html::a('Включить', ['enable', 'id' => $model->id], [
                            'class' => 'label label-success',
                            'title' => 'Нажмите, чтобы отключить.'
                        ]);
                    },
                ],
                'buttonOptions' => ['class' => 'btn btn-default']
            ],
        ],
    ]); ?>


</div>
