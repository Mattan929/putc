<?php

use floor12\files\components\FileListWidget;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CourseToUser */

$this->title = 'Запись номер ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Запись на курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$user = $model->user;
$name = $user->profile !== null ? $user->profile->name : $user->username;
?>
<div class="course-to-user-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Отказать', ['refuse', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Вы действительно хотите отказать?',
            ],
        ]) ?>
        <?= Html::a('Открыть доступ', ['open-access', 'id' => $model->id], [
            'class' => 'btn btn-success pull-right',
            'data' => [
                'confirm' => 'Вы уверены, что была оплата?',
            ],
        ]) ?>
        <?= Html::a('Подтвердить', ['approve', 'id' => $model->id], [
            'class' => 'btn btn-info pull-right',
            'data' => [
                'confirm' => 'Вы уверены, что все заполнено верно?',
            ],
        ]) ?>
    </p>
    <div class="row">
        <div class="col-md-6">
            <label>Данные пользователя</label>
            <?= DetailView::widget([
                'model' => $user,
                'attributes' => [
                    'username',
                    'email',
                    [
                        'attribute' => 'profile.name',
                        'label' => 'Фамилия Имя Отчество'
                    ],
                    [
                        'attribute' => 'profile.location',
                        'label' => 'Номер телефона'
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <label>Сканы паспорта</label>
            <?= FileListWidget::widget([
                'files' => $user->passport,
                'downloadAll' => true,
                'zipTitle' => "Passport of {$name}"
            ]) ?>
            <div class="row">
                <div class="col-md-4">
                    <?= FileListWidget::widget([
                        'files' => $model->application ? [$model->application] : $model->application,
                        'title' => 'Заявление',
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= FileListWidget::widget([
                        'files' => $model->confirmation ? [$model->confirmation] : $model->confirmation,
                        'title' => 'Подтверждение',
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= FileListWidget::widget([
                        'files' => $model->check ? [$model->check] : $model->check,
                        'title' => 'Чек об оплате',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <label>Информация по записи</label>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'course_id',
                'value' => $model->course->title
            ],
            [
                'attribute' => 'subject_id',
                'value' => $model->subject->title
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => '<label>' . $model->statusList[$model->status] . '</label>'
            ],
            [
                'attribute' => 'created_at',
                'value' => date('d.m.Y H:i:s', $model->created_at)
            ],
        ],
    ]) ?>

</div>
