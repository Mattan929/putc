<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CourseToUser */

$this->title = 'Записать на курс';
$this->params['breadcrumbs'][] = ['label' => 'Запись на курс', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-to-user-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
