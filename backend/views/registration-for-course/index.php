<?php

use common\models\Course;
use common\models\CourseToUser;
use common\models\Subject;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CourseToUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Запись на курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-to-user-index">

    <p>
        <?= Html::a('Ручная запись на предмет курса', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options' => [
                        'placeholder' => 'Выберите пользователя',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]),
                'value' => function ($model) {
                    return $model->user->username;
                }
            ],
            [
                'attribute' => 'course_id',
                'format' => 'raw',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'course_id',
                    'data' => ArrayHelper::map(Course::findAll(['status' => Course::STATUS_PUBLISHED]), 'id', 'title'),
                    'options' => [
                        'placeholder' => 'Выберите курс',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]),
                'value' => function ($model) {
                    return $model->course->title;
                }
            ],
            [
                'attribute' => 'subject_id',
                'format' => 'raw',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'subject_id',
                    'data' => ArrayHelper::map(Subject::findAll(['status' => Subject::STATUS_ACTIVE]), 'id', 'title'),
                    'options' => [
                        'placeholder' => 'Выберите предмет',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]),
                'value' => function ($model) {
                    return $model->subject->title;
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Дата записи',
                        'autocomplete' => 'off',
                    ],
                ]),
                'value' => function ($model) {
                    return date('d.m.Y H:i:s', $model->created_at);
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status', CourseToUser::getStatusList(), ['class' => 'form-control', 'prompt' => 'Все']),
                'value' => function ($model) {
                    return $model->statusList[$model->status];
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttonOptions' => ['class' => 'btn btn-default']
            ],
        ],
    ]); ?>


</div>
