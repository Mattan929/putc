<?php

use common\models\Course;
use common\models\Subject;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CourseToUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-to-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::className(), [
        'attribute' => 'user_id',
        'data' => ArrayHelper::map(\dektrium\user\models\User::find()->all(), 'id', 'username'),
        'pluginOptions' => [
            'allowClear' => true,
            'placeholder' => 'Выберите пользователя'
        ]
    ]) ?>

    <?= $form->field($model, 'course_id')->widget(\kartik\select2\Select2::className(), [
        'attribute' => 'course_id',
        'options' => [
            'id' => 'course-id',
        ],
        'data' => ArrayHelper::map(Course::findAll(['status' => Course::STATUS_PUBLISHED]), 'id', 'title'),
        'pluginOptions' => [
            'allowClear' => true,
            'placeholder' => 'Выберите курс'
        ]
    ]) ?>

    <?= $form->field($model, 'subject_id')->widget(DepDrop::classname(), [
        'options' => [
            'id' => 'subject-id'
        ],
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions' => [
            'depends' => ['course-id'],
            'placeholder' => 'Выберите предмет курса...',
            'url' => \yii\helpers\Url::to(['get-subject'])
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
