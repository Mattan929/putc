<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CourseToUser */

$this->title = 'Радактирование записи: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Запись на курс', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="course-to-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
