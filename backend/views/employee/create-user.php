<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model \dektrium\user\models\RegistrationForm */
/* @var $emploee \common\models\Employee */

$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin([
    'id' => 'registration-form',
]); ?>

<?= $form->field($model, 'name')->textInput(['value' => $emploee->fullName]) ?>

<?= $form->field($model, 'username') ?>

<?= $form->field($model, 'email') ?>

<?= $form->field($model, 'phone') ?>

<?= $form->field($model, 'password')?>

<?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-success btn-block']) ?>

<?php ActiveForm::end(); ?>
