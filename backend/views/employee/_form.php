<?php

use common\models\Subject;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .employee-form img {
        max-height: 200px;
    }
</style>
<div class="employee-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList(\common\models\Employee::getStatusList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'photo')->widget(floor12\files\components\FileInputWidget::class) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'subject_ids')->widget(\kartik\select2\Select2::class, [
                'data' => \yii\helpers\ArrayHelper::map(Subject::findAll(['status' => Subject::STATUS_ACTIVE]), 'id', 'title'),
                'options' => [
                    'placeholder' => 'Выберире предметы'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
