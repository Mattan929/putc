<?php


/* @var $this \yii\web\View */
/* @var $emploee \common\models\Employee */


use common\models\CourseToSubject;
use common\models\CourseToUser;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Курсы сотрудника';
$this->params['breadcrumbs'][] = $this->title;

$userToCourse = CourseToUser::findAll(['user_id' => $emploee->user_id]);
$courseToSubject = [];
foreach ($userToCourse as $record) {
    $courseToSubject[] =  $record->courseToSubject;
}
?>

<div class="row">
    <div class="col-md-12 text-center">
        <label>Назначить на курс с ролью "Преподаватель"</label>
    </div>
    <div class="col-md-12" data-role="subject-selector-container">
        <?php $form = ActiveForm::begin(); ?>
        <?= \kartik\select2\Select2::widget([
            'name' => 'MoodleCourseIds[]',
            'data' => ArrayHelper::map(CourseToSubject::find()->all(), 'id', 'subject.title', 'course.title'),
            'value' => ArrayHelper::getColumn($courseToSubject, 'id'),
            'options' => [
                'placeholder' => 'Выберите предметы'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ]
        ]) ?>
        <br>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>

