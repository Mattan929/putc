<?php

use common\models\Employee;
use common\models\Subject;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <p>
        <?= Html::a('Добавить сотрудника', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'surname',
            'name',
            'patronymic',
            [
                'attribute' => 'subject_ids',
                'format' => 'raw',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'subject_ids',
                    'data' => ArrayHelper::map(Subject::findAll(['status' => Subject::STATUS_ACTIVE]), 'id', 'title'),
                    'options' => [
                        'placeholder' => 'Введите предмет',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]),
                'value' => function ($model) {
                    $out = '';
                    foreach ($model->subjects as $subject) {
                        $out .= Html::tag('span', $subject->title, [
                            'class' => 'label label-primary',
                        ]);
                        $out .= '<br>';
                    }
                    return $out;
                }
            ],
            'position',
            'experience',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status', Employee::getStatusList(), ['class'=>'form-control', 'prompt' => 'Все']),
                'value' => function ($model) {
                    if ($model->status === Employee::STATUS_ACTIVE) {
                        return Html::tag('span', 'активно', [
                            'class' => 'label label-success',
                        ]);
                    }
                    return Html::tag('span', 'не активно', [
                        'class' => 'label label-danger',
                    ]);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{link} {action} {course} {update} {delete}',
                'buttons' => [
                    'action' => function($url, $model, $key){
                        if ($model->status === Employee::STATUS_ACTIVE){
                            return Html::a('Отключить', ['disable', 'id' => $model->id], [
                                'class' => 'label label-danger',
                                'title' => 'Нажмите, чтобы включить.',
                                'data-confirm' => 'Вы действительно хотите отключить данный элемент?'
                            ]);
                        }
                        return Html::a('Включить', ['enable', 'id' => $model->id], [
                            'class' => 'label label-success',
                            'title' => 'Нажмите, чтобы отключить.'
                        ]);
                    },
                    'link' => function($url, $model, $key){
                        if ($user = $model->user) {
                            return Html::tag('span', 'Пользователь: ' . $user->username, [
                                'class' => 'label label-default',
                                'title' => 'Пользователь'
                            ]);
                        }
                        return Html::a('Создать пользователя', ['create-user', 'id' => $model->id], [
                            'class' => 'label label-success',
                            'title' => 'Создать пользователя'
                        ]);
                    },
                    'course' => function($url, $model, $key){
                        return Html::a('Курсы сотрудника', ['course', 'id' => $model->id], [
                            'class' => 'btn btn-default',
                        ]);
                    },
                ],
                'buttonOptions' => ['class' => 'btn btn-default'],
            ],
        ],
    ]); ?>


</div>
