<?php

/* @var $this yii\web\View */
/* @var $model common\models\ScheduleAddress */

$this->title = 'Редактировать время: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="schedule-address-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
