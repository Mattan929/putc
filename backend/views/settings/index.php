<?php

use common\models\ScheduleAddress;
use common\models\ScheduleTime;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModelTime common\models\search\ScheduleTimeSearch */
/* @var $dataProviderTime yii\data\ActiveDataProvider */
/* @var $searchModelAddress common\models\search\ScheduleAddressSearch */
/* @var $dataProviderAddress yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-address-index row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Добавить настройку', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                'value',
                'code',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {remove}',
                    'buttons' => [
                        'remove' => function ($url, $model, $key) {
                            if (!$model->cant_be_removed) {
                                $url = Url::to(['settings/delete', 'id' => $key]);
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'class' => 'btn btn-default',
                                    'title' => \Yii::t('yii', 'Delete'),
                                    'data-confirm' => 'Вы действительно хотите удалить данный элемент?',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                            return '';
                        },
                    ],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'options' => ['style' => 'width: 125px;']
                ],
            ],
        ]); ?>
    </div>
    <div class="col-md-6">
        <div class="row">
            <?php $formTime = ActiveForm::begin(['action' => Url::to(['settings/create-time'])]); ?>
            <div class="col-sm-8 col-md-6 col-lg-8">
                <?= $formTime->field(new ScheduleTime(), 'time')
                    ->textInput(['maxlength' => true, 'placeholder' => 'Время'])
                    ->label(false) ?>
            </div>
            <div class="col-sm-4 col-md-6 col-lg-4">
                <div class="form-group">
                    <?= Html::submitButton('Добавить время', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="row">
            <?= GridView::widget([
                'dataProvider' => $dataProviderTime,
                'filterModel' => $searchModelTime,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'time',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'filter' => Html::activeDropDownList($searchModelAddress, 'status', ScheduleTime::getStatusList(), ['class' => 'form-control', 'prompt' => 'Все']),
                        'value' => function ($model) {
                            if ($model->status === ScheduleTime::STATUS_ACTIVE) {
                                return Html::tag('span', 'активно', [
                                    'class' => 'label label-success',
                                ]);
                            }
                            return Html::a('span', 'не активно', [
                                'class' => 'label label-danger',
                            ]);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'action' => function ($url, $model, $key) {
                                if ($model->status === ScheduleTime::STATUS_ACTIVE) {
                                    return Html::a('Отключить', ['disable-time', 'id' => $model->id], [
                                        'class' => 'label label-danger',
                                        'title' => 'Нажмите, чтобы включить.',
                                        'data-confirm' => 'Вы действительно хотите отключить данный элемент?'
                                    ]);
                                }
                                return Html::a('Включить', ['enable-time', 'id' => $model->id], [
                                    'class' => 'label label-success',
                                    'title' => 'Нажмите, чтобы отключить.'
                                ]);
                            },
                            'update' => function ($url, $model, $key) {
                                $url = Url::to(['settings/update-time', 'id' => $key]);
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'class' => 'btn btn-default',
                                    'title' => \Yii::t('yii', 'Delete'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            },
                            'remove' => function ($url, $model, $key) {
                                $url = Url::to(['settings/delete-time', 'id' => $key]);
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'class' => 'btn btn-default',
                                    'title' => \Yii::t('yii', 'Delete'),
                                    'data-confirm' => 'Вы действительно хотите удалить данный элемент?',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            },

                        ],
                        'template' => '{action} {update} {remove}',
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'options' => ['style' => 'width: 125px;']
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <div class="col-md-6">
        <div class="row">
            <?php $formAddress = ActiveForm::begin(['action' => Url::to(['settings/create-address'])]); ?>
            <div class="col-sm-8 col-md-6 col-lg-8">
                <?= $formAddress->field(new ScheduleAddress(), 'address')
                    ->textInput(['maxlength' => true, 'placeholder' => 'Адрес'])
                    ->label(false) ?>
            </div>
            <div class="col-sm-4 col-md-6 col-lg-4">
                <div class="form-group">
                    <?= Html::submitButton('Добавить адрес', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="row">
            <?= GridView::widget([
                'dataProvider' => $dataProviderAddress,
                'filterModel' => $searchModelAddress,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'address',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'filter' => Html::activeDropDownList($searchModelAddress, 'status', ScheduleAddress::getStatusList(), ['class' => 'form-control', 'prompt' => 'Все']),
                        'value' => function ($model) {
                            if ($model->status === ScheduleAddress::STATUS_ACTIVE) {
                                return Html::tag('span', 'активно', [
                                    'class' => 'label label-success',
                                ]);
                            }
                            return Html::a('span', 'не активно', [
                                'class' => 'label label-danger',
                            ]);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'action' => function ($url, $model, $key) {
                                if ($model->status === ScheduleAddress::STATUS_ACTIVE) {
                                    return Html::a('Отключить', ['disable-address', 'id' => $model->id], [
                                        'class' => 'label label-danger',
                                        'title' => 'Нажмите, чтобы включить.',
                                        'data-confirm' => 'Вы действительно хотите отключить данный элемент?'
                                    ]);
                                }
                                return Html::a('Включить', ['enable-address', 'id' => $model->id], [
                                    'class' => 'label label-success',
                                    'title' => 'Нажмите, чтобы отключить.'
                                ]);
                            },
                            'update' => function ($url, $model, $key) {
                                $url = Url::to(['settings/update-address', 'id' => $key]);
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'class' => 'btn btn-default',
                                    'title' => \Yii::t('yii', 'Delete'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            },
                            'remove' => function ($url, $model, $key) {
                                $url = Url::to(['settings/delete-address', 'id' => $key]);
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'class' => 'btn btn-default',
                                    'title' => \Yii::t('yii', 'Delete'),
                                    'data-confirm' => 'Вы действительно хотите удалить данный элемент?',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            },

                        ],
                        'template' => '{action} {update} {remove}',
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'options' => ['style' => 'width: 125px;']
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
