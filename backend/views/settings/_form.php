<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'readonly' => $model->cant_be_removed]) ?>

    <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'additional_value')->textarea(['rows' => 6]) ?>

    <?php if (Yii::$app->user->can('superadmin')) {
        echo $form->field($model, 'cant_be_removed')->checkbox();
    } ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'readonly' => $model->cant_be_removed]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
