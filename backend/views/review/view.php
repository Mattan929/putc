<?php


use common\models\Review;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Review */

$this->title = 'Отзыв от: ' . $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="review-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действидельно хотите удалить отзыв?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        if ($model->status === Review::STATUS_NEED_CHECK) {
            echo Html::a('Опубликовать', ['publish', 'id' => $model->id], ['class' => 'btn btn-success']);
        }
        if ($model->status === Review::STATUS_PUBLISH) {
            echo Html::a('Скрыть', ['hide', 'id' => $model->id], ['class' => 'btn btn-danger']);
        }
        if ($model->status === Review::STATUS_HIDDEN) {
            echo Html::a('Показать', ['publish', 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>
    </p>

    <label>ФИО:</label>
    <p><?= $model->fullname ?></p>
    <label>Текс отзыва:</label>
    <p><?= $model->content ?></p>
    <label>Дата создания:</label>
    <p><?= $model->createdDate ?></p>
    <label>Дата последнего изменения:</label>
    <p><?= $model->updatedDate ?></p>

</div>
