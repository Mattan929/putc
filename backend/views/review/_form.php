<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\Review::getStatusList()) ?>

    <?= $form->field($model, 'createdDate')->widget(DatePicker::className(), [
        'options' => [
            'placeholder' => 'Введите дату создания'
        ],
        'pluginOptions' => [
            'todayHighlight' => true,
//                    'startDate' => 'today',
//                'todayBtn' => true,
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'updatedDate')->widget(DatePicker::className(), [
        'options' => [
            'placeholder' => 'Введите дату обновления'
        ],
        'pluginOptions' => [
            'todayHighlight' => true,
//                    'startDate' => 'today',
//                'todayBtn' => true,
            'autoclose' => true
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
