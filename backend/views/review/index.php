<?php

use common\models\Review;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <p>
        <?= Html::a('Создать отзыв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fullname',
            [
                'attribute' => 'content',
                'contentOptions' => [
                    'style' => 'width: 500px; word-break: break-word;'
                ]
            ],
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    Review::getStatusList(),
                    ['class' => 'form-control', 'prompt' => 'Статус']
                ),
                'value' => function($model) {
                    $arrayTranslate = Review::getStatusList();
                    return $arrayTranslate[$model->status];
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Дата создания',
                        'autocomplete' => 'off',
                    ],
                ]),
                'value' => function($model) {
                    return date('d.m.Y H:i:s', $model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updated_at',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Дата обновления',
                        'autocomplete' => 'off',
                    ],
                ]),
                'value' => function($model) {
                    return date('d.m.Y H:i:s', $model->created_at);
                }
            ],
            [
                'header' => 'Действия',
                'value' => function($model) {
                    if ($model->status === Review::STATUS_NEED_CHECK) {
                        return Html::a('Опубликовать', ['publish', 'id' => $model->id], ['class' => 'label label-success']);
                    }
                    if ($model->status === Review::STATUS_PUBLISH) {
                        return Html::a('Скрыть', ['hide', 'id' => $model->id], ['class' => 'label label-danger']);
                    }
                    if ($model->status === Review::STATUS_HIDDEN) {
                        return Html::a('Показать', ['publish', 'id' => $model->id], ['class' => 'label label-primary']);
                    }
                },
                'format' => 'raw',
                'visible' => Yii::$app->user->can('moderator')
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttonOptions' => ['class' => 'btn btn-default'],
            ],
        ],
    ]); ?>


</div>
