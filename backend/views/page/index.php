<?php

use common\models\Page;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="post-index">

    <p>
        <?= Html::a('Добавить страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title:ntext',
            'slug',
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    Page::getStatusList(),
                    ['class' => 'form-control', 'prompt' => 'Статус']
                ),
                'value' => function($model) {
                    $arrayTranslate = Page::getStatusList();
                    return $arrayTranslate[$model->status];
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Дата создания',
                        'autocomplete' => 'off',
                    ],
                ]),
                'value' => function($model) {
                    return date('d.m.Y H:i:s', $model->created_at);
                }
            ],
            [
                'header' => 'Действия',
                'value' => function($model) {
                    if ($model->status === Page::STATUS_DRAFT) {
                        return Html::a('Опубликовать', ['publish', 'id' => $model->id], ['class' => 'label label-success']);
                    }
                    if ($model->status === Page::STATUS_PUBLISH) {
                        return Html::a('Скрыть', ['hide', 'id' => $model->id], ['class' => 'label label-danger']);
                    }
                    if ($model->status === Page::STATUS_HIDDEN) {
                        return Html::a('Показать', ['publish', 'id' => $model->id], ['class' => 'label label-primary']);
                    }
                },
                'format' => 'raw',
                'visible' => Yii::$app->user->can('moderator')
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'buttonOptions' => ['class' => 'btn btn-default'],
            ],
        ],
    ]); ?>
</div>
