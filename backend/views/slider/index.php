<?php

use common\models\Slider;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <p>
        <?= Html::a('Добавить слайд', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title:ntext',
            'url:ntext',
            'order',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status', Slider::getStatusList(), ['class' => 'form-control', 'prompt' => 'Все']),
                'value' => function ($model) {
                    if ($model->status === Slider::STATUS_ACTIVE) {
                        return Html::tag('span', 'ВКЛ', [
                            'class' => 'label label-success',
                        ]);
                    }
                    return Html::tag('span', 'ВЫКЛ', [
                        'class' => 'label label-danger',
                    ]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{action}{update}{delete}',
                'buttons' => [
                    'action' => function ($url, $model, $key) {
                        if ($model->status === Slider::STATUS_ACTIVE) {
                            return Html::a('Отключить', ['disable', 'id' => $model->id], [
                                'class' => 'label label-danger',
                                'title' => 'Нажмите, чтобы включить.',
                                'data-confirm' => 'Вы действительно хотите отключить данный элемент?'
                            ]);
                        }
                        return Html::a('Включить', ['enable', 'id' => $model->id], [
                            'class' => 'label label-success',
                            'title' => 'Нажмите, чтобы отключить.'
                        ]);
                    },
                ],
                'buttonOptions' => ['class' => 'btn btn-default']
            ],
        ],
    ]); ?>


</div>
