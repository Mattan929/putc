<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput() ?>
            <?= $form->field($model, 'url')->textInput() ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'order')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'status')->dropDownList(\common\models\Slider::getStatusList()) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(floor12\files\components\FileInputWidget::class) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
