<?php

use common\models\Course;
use common\models\Subject;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <p>
        <?= Html::a('Добавить курс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'subject_ids',
                'format' => 'raw',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'subject_ids',
                    'data' => ArrayHelper::map(Subject::find()->where(['status' => Subject::STATUS_ACTIVE])->all(), 'id', 'title'),
                    'options' => [
                        'placeholder' => 'Введите предмет',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]),
                'value' => function ($model) {
                    $out = '';
                    foreach ($model->subjects as $subject) {
                        $out .= Html::tag('span', $subject->title, [
                            'class' => 'label label-primary',
                        ]);
                        $out .= '<br>';
                    }
                    return $out;
                }
            ],
            'total_hours',
            [
                'attribute' => 'beginDate',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'type' => DatePicker::TYPE_INPUT,
                    'attribute' => 'beginDate',
                    'options' => ['placeholder' => 'Введите дату начала'],
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]),
                'content' => function ($model) {
                    if (!empty($model->beginDate)) {
                        return $model->beginDate;
                    }

                    return false;
                }
            ],
            [
                'attribute' => 'endDate',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'type' => DatePicker::TYPE_INPUT,
                    'attribute' => 'endDate',
                    'options' => ['placeholder' => 'Введите дату окончания'],
                    'pluginOptions' => [
                        'autoclose' => true,
                    ]
                ]),
                'content' => function ($model) {
                    if ($model->endDate) {
                        return $model->endDate;
                    }

                    return false;
                }
            ],
            'cost',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status', Course::getStatusList(), ['class'=>'form-control', 'prompt' => 'Все']),
                'value' => function ($model) {
                    if ($model->status === Course::STATUS_PUBLISHED) {
                        return Html::tag('span', 'Опубликовано', [
                            'class' => 'label label-success',
                        ]);
                    }
                    if ($model->status === Course::STATUS_DRAFT) {
                        return Html::tag('span', 'Черновик', [
                            'class' => 'label label-primary',
                        ]);
                    }
                    return Html::tag('span', 'Скрыт', [
                        'class' => 'label label-danger',
                    ]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{action} {import} {update} {delete}',
                'buttons' => [
                    'action' => function($url, $model, $key){
                        if ($model->status === Course::STATUS_PUBLISHED) {
                            return Html::a('Скрыть', ['unpublish', 'id' => $model->id], [
                                'class' => 'label label-danger',
                                'title' => 'Нажмите, чтобы скрыть.',
                                'data-confirm' => 'Вы действительно хотите скрыть данный курс?'
                            ]);
                        }
                        return Html::a('Опубликовать', ['publish', 'id' => $model->id], [
                            'class' => 'label label-success',
                            'title' => 'Нажмите, чтобы опубликовать.',
                        ]);
                    },
                    'import' => function($url, $model, $key){
                        if ($model->moodle_category_id === null) {
                            return Html::a('Импорт', ['import', 'id' => $model->id], [
                                'class' => 'label label-info',
                                'title' => 'Нажмите, чтобы импортировать в Moodle.',
                            ]);
                        }
                        return '';
                    },
                ],
                'buttonOptions' => ['class' => 'btn btn-default']
            ],
        ],
    ]); ?>


</div>
