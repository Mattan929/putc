<?php

use common\models\Subject;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'subject_ids')->widget(\kartik\select2\Select2::className(), [
                'data' => ArrayHelper::map(Subject::find()->where(['status' => Subject::STATUS_ACTIVE])->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Введите предметы'],
                'pluginOptions' => [
                    'multiple' => true,
                    'autoclose' => true
                ]
            ]) ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'total_hours')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'hours_in_month')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'hours_in_week')->textInput() ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'beginDate')->widget(DatePicker::className(), [
                'options' => [
                    'placeholder' => 'Введите дату начала'
                ],
                'pluginOptions' => [
                    'todayHighlight' => true,
//                    'startDate' => 'today',
//                'todayBtn' => true,
                    'autoclose' => true
                ]
            ]) ?>

            <?= $form->field($model, 'endDate')->widget(DatePicker::className(), [
                'options' => [
                    'placeholder' => 'Введите дату окончания'
                ],
                'pluginOptions' => [
                    'todayHighlight' => true,
//                    'startDate' => 'today',
//                'todayBtn' => true,
                    'autoclose' => true
                ]
            ]) ?>

            <?= $form->field($model, 'cost')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
