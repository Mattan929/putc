<?php

use common\models\Post;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Файлы';
$this->params['breadcrumbs'][] = $this->title;

?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Файл',
            'format' => 'raw',
            'value' => function ($model) {
                return \floor12\files\components\FileListWidget::widget(['files' => [$model]]);
            }
        ],
        [
            'label' => 'Модель',
            'attribute' => 'class'
        ],
        [
            'label' => 'Имя файла',
            'attribute' => 'title'
        ],
        [
            'label' => 'Тип',
            'attribute' => 'content_type'
        ],
//        [
//            'class' => 'yii\grid\ActionColumn',
//            'template' => '{update}{delete}',
//            'buttonOptions' => ['class' => 'btn btn-default'],
//        ],
    ],
]); ?>
