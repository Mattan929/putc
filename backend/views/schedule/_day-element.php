<?php

/* @var $this \yii\web\View */

use common\models\ScheduleRecord;
use yii\helpers\ArrayHelper;

/* @var $day_id int */
/* @var $schedule_id integer */
/* @var $course \common\models\Course */
/* @var $element \common\models\ScheduleRecord */

?>

<div class="form-group schedule-record-block" data-role="schedule-record-block">
    <input type="hidden" name="ScheduleRecords[<?= $day_id ?>][<?= $key ?>][day_id]" value="<?= $day_id ?>">
    <input type="hidden" name="ScheduleRecords[<?= $day_id ?>][<?= $key ?>][schedule_id]" value="<?= $schedule_id ?>">
    <div class="row form-group">
        <div class="col-md-3">
            <?= \yii\helpers\Html::dropDownList(
                'ScheduleRecords[' . $day_id . '][' . $key . '][time_id]',
                $element->time_id,
                ScheduleRecord::getTimeList(), ['class' => 'form-control', 'prompt' => 'Выберите время'])
            ?>
        </div>
        <div class="col-md-5">
            <?= \kartik\select2\Select2::widget([
                'name' => 'ScheduleRecords[' . $day_id . '][' . $key . '][subject_id]',
                'value' => $element->subject_id,
                'id' => uniqid('subject_', ''),
                'data' => ArrayHelper::map($course->subjects, 'id', 'title'),
                'options' => [
                    'placeholder' => 'Выберите предмет'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <a class="btn btn-success" data-role="add-new-schedule-record" href="javascript:" title="Добавить еще"><i class="fa fa-plus"></i></a>
            <a class="btn btn-danger" data-role="remove-schedule-record" href="javascript:" title="Удалить"><i class="fa fa-minus"></i></a>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-8">
            <?= \yii\helpers\Html::dropDownList(
                'ScheduleRecords[' . $day_id . '][' . $key . '][address_id]',
                $element->address_id,
                ScheduleRecord::getAddressList(), ['class' => 'form-control', 'prompt' => 'Выберите адрес'])
            ?>
        </div>
        <div class="col-md-4">
            <input class="form-control"
                   type="text"
                   name="ScheduleRecords[<?= $day_id ?>][<?= $key ?>][classroom]"
                   value="<?= $element->classroom ?>"
                   placeholder="Аудитория">
        </div>
    </div>
</div>
