<?php

use common\models\Course;
use common\models\ScheduleRecord;
use common\models\Subject;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'course_id')->widget(\kartik\select2\Select2::class, [
                'data' => ArrayHelper::map(Course::findAll(['status' => Course::STATUS_PUBLISHED]), 'id', 'title'),
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status')->dropDownList(\common\models\Schedule::getStatusList()) ?>
        </div>
    </div>

    <?php
    if (!$model->isNewRecord) { ?>
        <div class="row">
            <?php
            /** @var Course $course */
            $days = $model->getScheduleRecordsArray();
            foreach ($days as $day => $dayElements) {
                if ($day % 2 !== 0){
                    echo '<div class="clearfix"></div>';
                }
                ?>
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-header">
                            <h4 class="text-center"><?= ScheduleRecord::getDayList()[$day] ?></h4>
                        </div>
                        <div class="panel-body"
                             data-role="settings"
                             data-next-key="<?= count($dayElements) + 1 ?>"
                             data-schedule-id="<?= $model->id ?>"
                             data-day-id="<?= $day ?>"
                             data-add-url="<?= \yii\helpers\Url::toRoute('add-new-schedule-record') ?>">

                            <div class="row">
                                <div class="col-md-3">
                                    <label>Время</label>
                                </div>
                                <div class="col-md-5">
                                    <label>Предмет</label>
                                </div>
                                <div class="col-md-4">
                                    <label>Действия</label>
                                </div>
                            </div>
                            <?php
                            foreach ($dayElements as $key => $element){
                                echo $this->render('_day-element', [
                                    'element' => $element,
                                    'day_id' => $day,
                                    'schedule_id' => $model->id,
                                    'course' => $model->course,
                                    'key' => $key
                                ]);
                            }
                            if (empty($dayElements)) {
                              echo $this->render('_day-element', [
                                  'element' => new ScheduleRecord(),
                                  'day_id' => $day,
                                  'schedule_id' => $model->id,
                                  'course' => $model->course,
                                  'key' => 0
                              ]);
                            } ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
