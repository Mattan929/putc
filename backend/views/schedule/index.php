<?php

use common\models\Course;
use common\models\Schedule;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расписание';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-index">

    <p>
        <?= Html::a('Создать расписание', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'course_id',
                'format' => 'raw',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'course_id',
                    'data' => ArrayHelper::map(Course::find()->where(['status' => Course::STATUS_PUBLISHED])->all(), 'id', 'title'),
                    'options' => [
                        'placeholder' => 'Введите название курса',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]),
                'value' => function ($model) {
                    $course = $model->course;
                    return $course !== null ? $course->title : 'Курс не задан';
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status', Schedule::getStatusList(), ['class'=>'form-control', 'prompt' => 'Все']),
                'value' => function ($model) {
                    if ($model->status === Schedule::STATUS_ACTIVE){
                        return Html::tag('span', 'ВКЛ', [
                            'class' => 'label label-success',
                        ]);
                    }
                    return Html::tag('span', 'ВЫКЛ', [
                        'class' => 'label label-danger',
                    ]);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{action}{update}{delete}',
                'buttons' => [
                    'action' => function($url, $model, $key){
                        if ($model->status === Schedule::STATUS_ACTIVE){
                            return Html::a('Отключить', ['disable', 'id' => $model->id], [
                                'class' => 'label label-danger',
                                'title' => 'Нажмите, чтобы включить.',
                                'data-confirm' => 'Вы действительно хотите отключить данный элемент?'
                            ]);
                        }
                        return Html::a('Включить', ['enable', 'id' => $model->id], [
                            'class' => 'label label-success',
                            'title' => 'Нажмите, чтобы отключить.'
                        ]);
                    },
                ],
                'buttonOptions' => ['class' => 'btn btn-default']
            ],
        ],
    ]); ?>


</div>
