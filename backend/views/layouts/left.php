<?php

use common\models\CourseToUser;

$newReviewCount = \common\models\Review::find()->where(['status' => \common\models\Review::STATUS_NEED_CHECK])->count();
$reviewBadge = '';
if ($newReviewCount > 0) {
    $reviewBadge = '<span class="label pull-right bg-yellow">' . $newReviewCount . '</span>';
}
$newRegToCourseCount = CourseToUser::find()->where(['status' => CourseToUser::STATUS_NEW])->count();
$newRegToCourseBadge = '';
if ($newRegToCourseCount > 0) {
    $newRegToCourseBadge = '<span class="label pull-right bg-yellow">' . $newRegToCourseCount . '</span>';
}
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Запись на курсы', 'template' => '<a href="{url}">{icon} {label} ' . $newRegToCourseBadge . '</a>', 'icon' => 'rub', 'url' => ['/registration-for-course'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Курсы', 'icon' => 'euro', 'url' => ['/course'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Предметы', 'icon' => 'book', 'url' => ['/subject'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Расписание', 'icon' => 'calendar', 'url' => ['/schedule'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Сотрудники', 'icon' => 'users', 'url' => ['/employee'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Новости', 'icon' => '', 'url' => ['/post'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Страницы', 'icon' => '', 'url' => ['/page'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Отзывы', 'template' => '<a href="{url}">{icon} {label} ' . $reviewBadge . '</a>', 'icon' => '', 'url' => ['/review'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Слайдер', 'icon' => 'image', 'url' => ['/slider'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Документы', 'icon' => 'file', 'url' => ['/documents'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Настройки', 'icon' => 'cogs', 'url' => ['/settings'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Файлы', 'icon' => '', 'url' => ['/document'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/user/admin'], 'visible' => Yii::$app->user->can('moderator')],
                ],
            ]
        ) ?>

    </section>

</aside>
