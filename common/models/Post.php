<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 * @property string|null $description
 * @property int|null $status
 * @property int|null $publish_date
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 */
class Post extends ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISH = 1;
    const STATUS_HIDDEN = 2;


    function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'files' => [
                'class' => 'floor12\files\components\FileBehaviour',
                'attributes' => [
                    'post_image',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'description'], 'required'],
            ['post_image', 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif'], 'maxFiles' => 1],
            [['title', 'content', 'description'], 'string'],
            [['status', 'created_at', 'publish_date', 'updated_at', 'created_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Контент',
            'description' => 'Короткое описание',
            'status' => 'Статус',
            'publish_date' => 'Опубликовано',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
            'post_image' => 'Изображение',
        ];
    }

    public static function getStatusList()
    {
        return [self::STATUS_DRAFT => 'Черновик', self::STATUS_PUBLISH => 'Опубликован', self::STATUS_HIDDEN => 'Скрыт'];
    }

    public function publish()
    {
        $this->status = self::STATUS_PUBLISH;
        $this->publish_date = time();
        return $this->save();
    }

    public function hide()
    {
        $this->publish_date = null;
        $this->status = self::STATUS_HIDDEN;
        return $this->save();
    }
}
