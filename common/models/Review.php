<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "review".
 *
 * @property int $id
 * @property string $fullname
 * @property string $content
 * @property int|null $status
 * @property int|null $created_at
 * @property string $updatedDate
 * @property string $createdDate
 * @property int|null $updated_at
 */
class Review extends ActiveRecord
{
    const STATUS_HIDDEN = 0;
    const STATUS_NEED_CHECK = 1;
    const STATUS_PUBLISH = 2;

    function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
//            'blameable' => [
//                'class' => BlameableBehavior::class,
//                'createdByAttribute' => 'created_by',
//                'updatedByAttribute' => 'updated_by',
//            ],
//            'files' => [
//                'class' => 'floor12\files\components\FileBehaviour',
//                'attributes' => [
//                    'post_image',
//                ],
//            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'content'], 'required'],
            [['content'], 'string'],
            [['updatedDate', 'createdDate'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['fullname'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'ФИО',
            'content' => 'Текст',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'createdDate' => 'Создано',
            'updatedDate' => 'Обновлено',
        ];
    }

    public static function getStatusList()
    {
        return [self::STATUS_NEED_CHECK => 'Нужна проверка', self::STATUS_PUBLISH => 'Опубликован', self::STATUS_HIDDEN => 'Скрыт'];
    }

    public function publish()
    {
        $this->status = self::STATUS_PUBLISH;
        return $this->save();
    }

    public function hide()
    {
        $this->status = self::STATUS_HIDDEN;
        return $this->save();
    }

    public function getCreatedDate()
    {
        return $this->created_at !== null ? date('d.m.Y', $this->created_at) : '';
    }

    public function getUpdatedDate()
    {
        return $this->updated_at !== null ? date('d.m.Y', $this->updated_at) : '';
    }

    public function setCreatedDate($date)
    {
        $this->created_at = strtotime($date);
        return true;
    }

    public function setUpdatedDate($date)
    {
        $this->updated_at = strtotime($date);

        return true;
    }
}
