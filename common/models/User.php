<?php
namespace common\models;

use common\components\llagerlof\moodlerest\models\MoodleUser;
use common\components\llagerlof\moodlerest\MoodleRest;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends \dektrium\user\models\User
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    const MOODLE_ROLE_MANAGER = 1;
    const MOODLE_ROLE_COURSECREATOR = 2;
    const MOODLE_ROLE_EDITINGTEACHER = 3;
    const MOODLE_ROLE_TEACHER = 4;
    const MOODLE_ROLE_STUDENT = 5;
    const MOODLE_ROLE_GUEST = 6;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            TimestampBehavior::className(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param $idnumber
     * @return mixed
     */
    public function getMoodleUserId()
    {
        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;

        $response = $moodleRest->request('core_user_get_users', [
            'criteria' => [
                [
                    'key' => 'email',
                    'value' => $this->email
                ]
            ]
        ]);

        if (!isset($response['users']) || empty($response['users'])) {
            return '';
        }

        return $response['users'][0]['id'];
    }

    /**
     * @param $courseId
     * @param null $roleId
     * @return mixed
     */
    public function enrol($courseId, $roleId = null)
    {

        if ($roleId === null) {
            $roleId = $this->getMoodleRoleId();
        }

        $mUserId = $this->getMoodleUserId();
        if ($mUserId === '') {
            $fullname = explode(' ', $this->profile->name);
            $mUser = new MoodleUser();
            $mUser->username = strtolower($this->email);
            $mUser->createpassword = 1;
            $mUser->lastname = isset($fullname[1]) && !empty($fullname[1]) ? $fullname[1] : 'не указано';
            $mUser->firstname = isset($fullname[0]) && !empty($fullname[0]) ? $fullname[0] : 'не указано';
            $mUser->email = strtolower($this->email);

            $mUser->create();

            $mUserId = $this->getMoodleUserId();
        }

        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;

        $response = $moodleRest->request('enrol_manual_enrol_users', [
            'enrolments' => [
                [
                    'roleid' => (integer)$roleId,
                    'userid' => (integer)$mUserId,
                    'courseid' => (integer)$courseId
                ]
            ]
        ]);

        if (isset($response['exception'])) {
            throw new HttpException(404, $response['message']);
        }
        return $response;

    }

    public function unenrol($courseId, $roleId = null)
    {
        $mUserId = $this->getMoodleUserId();

        if (empty($mUserId)) {
            throw new HttpException(404, 'Не найден пользователь в moodle');
        }

        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;

        $response = $moodleRest->request('enrol_manual_unenrol_users', [
            'enrolments' => [
                [
                    'roleid' => (integer)$roleId,
                    'userid' => (integer)$mUserId,
                    'courseid' => (integer)$courseId
                ]
            ]
        ]);
        if (isset($response['exception'])) {
            return false;
        }
        return true;
    }

    public function getMoodleRoleId()
    {
        switch (\Yii::$app->authManager->getRolesByUser($this->id)) {
            case 'editingteacher' :
                return self::MOODLE_ROLE_EDITINGTEACHER;
            case 'teacher' :
                return self::MOODLE_ROLE_TEACHER;
            case 'student' :
                return self::MOODLE_ROLE_STUDENT;
            case 'superadmin' :
                return self::MOODLE_ROLE_MANAGER;
            case 'moderator' :
                return self::MOODLE_ROLE_COURSECREATOR;
            default:
                return self::MOODLE_ROLE_GUEST;
        }
    }

    /**
     * @param $items
     * @return bool
     */
    public function assign($items, $roleId = null)
    {
        CourseToUser::deleteAll(['user_id' => $this->id]);
        foreach ($items as $mCourseId) {
            $this->enrol($mCourseId, $roleId);
            $subjectToMCourse = new CourseToUser();
            $subjectToMCourse->user_id = $this->id;
            $subjectToMCourse->moodle_course_id = $mCourseId;
            $subjectToMCourse->save();
        }
        return true;
    }
}
