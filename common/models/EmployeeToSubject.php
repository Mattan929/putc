<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "employee_to_subject".
 *
 * @property int $id
 * @property int|null $employee_id
 * @property int|null $subject_id
 */
class EmployeeToSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_to_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'subject_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'subject_id' => 'Subject ID',
        ];
    }
}
