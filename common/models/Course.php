<?php

namespace common\models;

use common\components\llagerlof\moodlerest\MoodleRest;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $subject_ids
 * @property int|null $status
 * @property int|null $total_hours
 * @property int|null $hours_in_month
 * @property int|null $hours_in_week
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $begin_date
 * @property int|null $end_date
 * @property int|null $moodle_category_id
 * @property float|null $cost
 *
 * @property \yii\db\ActiveQuery $subjects
 * @property string $endDate
 * @property string $beginDate
 * @property-write mixed $category
 * @property Subject $subject
 */
class Course extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISHED = 2;
    const STATUS_DRAFT = 1;
    const STATUS_UNPUBLISHED = 0;

    function behaviors()
    {
        return ArrayHelper::merge([
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
//            'blameable' => [
//                'class' => BlameableBehavior::class,
//                'createdByAttribute' => 'created_by',
//                'updatedByAttribute' => 'updated_by',
//            ],
            'relations' => [
                'class' => \voskobovich\linker\LinkerBehavior::className(),
                'relations' => [
                    'subject_ids' => 'subjects',
                ],
            ]
        ], parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'moodle_category_id', 'total_hours', 'hours_in_month', 'hours_in_week', 'created_at', 'updated_at', 'begin_date', 'end_date'], 'integer'],
            [['cost'], 'number'],
            [['subject_ids'], 'each', 'rule' => ['integer']],
            [['title', 'beginDate', 'endDate'], 'string', 'max' => 255],
            [['beginDate', 'endDate', 'subject_ids', 'title'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'subject_ids' => 'Предметы',
            'status' => 'Статус',
            'total_hours' => 'Всего часов',
            'hours_in_month' => 'Часов в месяц',
            'hours_in_week' => 'Часов в неделю',
            'created_at' => 'Создано в',
            'updated_at' => 'Обновлено в',
            'begin_date' => 'Дата начала',
            'beginDate' => 'Дата начала',
            'end_date' => 'Дата завершения',
            'endDate' => 'Дата завершения',
            'cost' => 'Стоимость',
            'moodle_category_id' => 'ID категории в moodle'
        ];
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subject::className(), ['id' => 'subject_id'])
            ->viaTable(CourseToSubject::tableName(), ['course_id' => 'id']);
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_UNPUBLISHED => 'Скрыто',
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];
    }

    public function publish()
    {
        $this->status = self::STATUS_PUBLISHED;
        return $this->save();
    }

    public function unpublish()
    {
        $this->status = self::STATUS_UNPUBLISHED;
        return $this->save();
    }

    public function getBeginDate()
    {
        return $this->begin_date !== null ? date('d.m.Y', $this->begin_date) : '';
    }

    public function getEndDate()
    {
        return $this->end_date !== null ? date('d.m.Y', $this->end_date) : '';
    }

    public function setBeginDate($date)
    {
        $this->begin_date = strtotime($date);
        return true;
    }

    public function setEndDate($date)
    {
        $this->end_date = strtotime($date);

        return true;
    }
    public function addMoodleCategory()
    {
        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;
        $response = $moodleRest->request('core_course_get_categories', [
            'criteria' => [
                [
                    'key' => 'name',
                    'value' => $this->title
                ]
            ]
        ]);

        if (isset($response['exception'])) {
            var_dump($response);
            die;
        }
        if (empty($response)) {
            /** @var MoodleRest $moodleRest */
            $moodleRest = Yii::$app->moodleRest;
            $response = $moodleRest->request('core_course_create_categories', [
                'categories' => [
                    [
                        'name' => $this->title,
                        'parent' => Settings::getValue('main-moodle-category')
                    ]
                ]
            ]);
        }

        return $response[0]['id'];
    }

    public function setCategory($categoryId)
    {
        $this->moodle_category_id = $categoryId;

        return $this->save();
    }
}
