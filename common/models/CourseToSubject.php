<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "course_to_subject".
 *
 * @property int $id
 * @property int|null $course_id
 * @property int|null $subject_id
 * @property int|null $moodle_course_id
 */
class CourseToSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_to_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'subject_id', 'moodle_course_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'subject_id' => 'Subject ID',
            'moodle_course_id' => 'Moodle Course ID',
        ];
    }

    public static function getCourseUrl($courseId, $subjectId)
    {
        $model = self::findOne(['course_id' => $courseId, 'subject_id' => $subjectId]);
        if ($model) {
            return 'https://de.kgsu.ru/course/view.php?id=' . $model->moodle_course_id;
        }
        return '#';
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourseToUser()
    {
        return $this->hasOne(CourseToUser::className(), ['course_id' => 'course_id', 'subject_id' => 'subject_id']);
    }
}
