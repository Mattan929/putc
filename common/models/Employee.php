<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $name
 * @property string $patronymic
 * @property string $surname
 * @property string $position
 * @property float|null $experience
 * @property string|null $description
 * @property-read \yii\db\ActiveQuery $user
 * @property-read string $fullName
 * @property-read \yii\db\ActiveQuery $subjects
 * @property int|null $status
 * @property int|null $user_id
 */
class Employee extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    function behaviors()
    {
        return [
            'files' => [
                'class' => 'floor12\files\components\FileBehaviour',
                'attributes' => [
                    'photo',
                ],
            ],
            'relations' => [
                'class' => \voskobovich\linker\LinkerBehavior::className(),
                'relations' => [
                    'subject_ids' => 'subjects',
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'patronymic', 'surname', 'position'], 'required'],
            ['photo', 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif'], 'maxFiles' => 1],
            [['experience'], 'number'],
            [['subject_ids'], 'each', 'rule' => ['integer']],
            [['description'], 'string'],
            [['status', 'user_id'], 'integer'],
            [['name', 'patronymic', 'surname', 'position'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
            'surname' => 'Фамилия',
            'position' => 'Должность',
            'experience' => 'Стаж',
            'description' => 'Описание',
            'status' => 'Статус',
            'photo' => 'Фото',
            'subject_ids' => 'Предметы',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subject::className(), ['id' => 'subject_id'])
            ->viaTable(EmployeeToSubject::tableName(), ['employee_id' => 'id']);
    }

    public function getFullName()
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'активно',
            self::STATUS_INACTIVE => 'не активно',
        ];
    }

    public function enable()
    {
        $this->status = self::STATUS_ACTIVE;
        return $this->save();
    }

    public function disable()
    {
        $this->status = self::STATUS_INACTIVE;
        return $this->save();
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
