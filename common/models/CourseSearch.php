<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Course;

/**
 * CourseSearch represents the model behind the search form of `common\models\Course`.
 */
class CourseSearch extends Course
{
    public $beginDate;
    public $endDate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'total_hours', 'hours_in_month',
                'hours_in_week', 'created_at', 'updated_at', 'begin_date', 'end_date'], 'integer'],
            [['title', 'beginDate', 'endDate', 'subject_ids'], 'safe'],
            [['cost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'status' => $this->status,
            'total_hours' => $this->total_hours,
            'hours_in_month' => $this->hours_in_month,
            'hours_in_week' => $this->hours_in_week,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
//            'begin_date' => $this->begin_date,
//            'end_date' => $this->end_date,
            'cost' => $this->cost,
        ]);

        if ($this->subject_ids) {
             $query->joinWith(['subjects']);
            $query->andWhere(['subject.id' => $this->subject_ids]);
        }

        if ($this->beginDate) {
            $query->andFilterWhere(['between', 'begin_date', strtotime($this->beginDate), strtotime($this->beginDate . ' 23:59:59')]);
        }
        if ($this->endDate) {
            $query->andFilterWhere(['between', 'end_date', strtotime($this->endDate), strtotime($this->endDate . ' 23:59:59')]);
        }

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
