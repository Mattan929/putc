<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "schedule".
 *
 * @property int $id
 * @property string $title
 * @property int $course_id
 * @property int|null $status
 *
 * @property \yii\db\ActiveQuery $course
 * @property ScheduleRecord[] $scheduleRecords
 */
class Schedule extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'course_id'], 'required'],
            [['course_id', 'status'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'course_id' => 'Курс',
            'status' => 'Статус',
        ];
    }

    /**
     * Gets query for [[ScheduleRecords]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScheduleRecords()
    {
        return $this->hasMany(ScheduleRecord::className(), ['schedule_id' => 'id']);
    }

    public function getScheduleRecordsArray()
    {
        $array = [
            1 => [],
            2 => [],
            3 => [],
            4 => [],
            5 => [],
            6 => []
        ];
        $records = $this->scheduleRecords;
        foreach ($records as $record){
            $array[$record->day_id][] = $record;
        }
        foreach ($array as $key => $arr) {
            $day = $arr;
            usort($day, function ($a, $b){
                if ($a->time === null || $b->time === null) {
                    return 0;
                }
                if (strtotime($a->time->time) === strtotime($b->time->time)) {
                    return 0;
                }
                return strtotime($a->time->time) > strtotime($b->time->time) ? 1 : -1;
            });
            $array[$key] = $day;
        }

        return $array;
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'вкл',
            self::STATUS_INACTIVE => 'выкл',
        ];
    }

    public function enable()
    {
        $this->status = self::STATUS_ACTIVE;
        return $this->save();
    }

    public function disable()
    {
        $this->status = self::STATUS_INACTIVE;
        return $this->save();
    }
}
