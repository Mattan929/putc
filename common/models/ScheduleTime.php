<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "schedule_time".
 *
 * @property int $id
 * @property string $time
 * @property int|null $status
 */
class ScheduleTime extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schedule_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time'], 'required'],
            [['status'], 'integer'],
            [['time'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Время',
            'status' => 'Статус',
        ];
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'активно',
            self::STATUS_INACTIVE => 'не активно',
        ];
    }

    public function enable()
    {
        $this->status = self::STATUS_ACTIVE;
        return $this->save();
    }

    public function disable()
    {
        $this->status = self::STATUS_INACTIVE;
        return $this->save();
    }

    public static function getTimeList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'time');
    }
}
