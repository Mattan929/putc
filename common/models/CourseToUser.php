<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course_to_user".
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property int $subject_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Course $course
 * @property Subject $subject
 * @property-read string[] $statusList
 * @property-read \yii\db\ActiveQuery $courseToSubject
 * @property User $user
 */
class CourseToUser extends \yii\db\ActiveRecord
{
    const STATUS_REFUSED = 0;
    const STATUS_NEW = 1;
    const STATUS_APPROVED = 2;
    const STATUS_PAYMENT = 3;

    function behaviors()
    {
        return ArrayHelper::merge([
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
            'files' => [
                'class' => 'floor12\files\components\FileBehaviour',
                'attributes' => [
                    'application',
                    'confirmation',
                    'check'
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_to_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'subject_id'], 'required'],
            [['user_id', 'course_id', 'subject_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['application', 'confirmation', 'check'], 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif', 'pdf', 'doc', 'docx'], 'maxFiles' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер записи',
            'user_id' => 'Пользователь',
            'course_id' => 'Курс',
            'subject_id' => 'Предмет',
            'status' => 'Статус',
            'created_at' => 'Дата записи',
            'updated_at' => 'Дата обновления',
            'application' => 'Заявление',
            'passport' => 'Сканы паспорта',
            'confirmation' => 'Согласием на обработку персональных данных',
            'check' => 'Документ, подтверждающий факт оплаты'
        ];
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourseToSubject()
    {
        return $this->hasOne(CourseToSubject::className(), ['course_id' => 'course_id', 'subject_id' => 'subject_id']);
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_REFUSED => 'Отказ',
            self::STATUS_NEW => 'Ожидает подтверждения',
            self::STATUS_APPROVED => 'Подтвержден',
            self::STATUS_PAYMENT=> 'Оплачено',
        ];
    }
}
