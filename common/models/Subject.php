<?php

namespace common\models;

use floor12\files\models\File;
use Yii;

/**
 * This is the model class for table "subject".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $short_title
 * @property int|null $status
 * @property File $image
 */
class Subject extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    function behaviors()
    {
        return [
//            'timestamp' => [
//                'class' => TimestampBehavior::className(),
//                'attributes' => [
//                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
//                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
//                ],
//            ],
//            'blameable' => [
//                'class' => BlameableBehavior::class,
//                'createdByAttribute' => 'created_by',
//                'updatedByAttribute' => 'updated_by',
//            ],
            'files' => [
                'class' => 'floor12\files\components\FileBehaviour',
                'attributes' => [
                    'image',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            ['image', 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif'], 'maxFiles' => 1],
            [['title', 'short_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'short_title' => 'Короткое название',
            'status' => 'Статус',
            'image' => 'Изображение',
        ];
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'вкл',
            self::STATUS_INACTIVE => 'выкл',
        ];
    }

    public function enable()
    {
        $this->status = self::STATUS_ACTIVE;
        return $this->save();
    }

    public function disable()
    {
        $this->status = self::STATUS_INACTIVE;
        return $this->save();
    }
}
