<?php

namespace common\models;

use common\components\llagerlof\moodlerest\MoodleRest;
use Tightenco\Collect\Support\Arr;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class MoodleCourse extends Model
{
    public $id;
    public $fullname;
    public $shortname;
    public $categoryid;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public function scenarios()
    {
//        $fields = array_keys($this->attributeLabels());
        return [
            self::SCENARIO_DEFAULT => ['fullname', 'shortname', 'categoryid'],
            self::SCENARIO_CREATE => ['fullname', 'shortname', 'categoryid'],
            self::SCENARIO_UPDATE => ['fullname', 'shortname', 'categoryid', 'id'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'shortname', 'categoryid'], 'required'],
            [['fullname', 'shortname', 'categoryid'], 'string', 'on' => self::SCENARIO_DEFAULT],
            [['fullname', 'shortname', 'categoryid', 'id'], 'string', 'on' => self::SCENARIO_UPDATE],
            [['fullname', 'shortname', 'categoryid'], 'string', 'on' => self::SCENARIO_CREATE],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Полное название',
            'shortname' => 'Короткое название',
            'categoryid' => 'Категория',
        ];
    }

    public static function getCategoryList($categoryId = null)
    {
        $moodleRest = Yii::$app->moodleRest;
        $criteria = [];
        if ($categoryId !== null) {
            $criteria = [
                ['key' => 'id', 'value' => $categoryId]
            ];
        }
        $categories = $moodleRest->request('core_course_get_categories', [
            'criteria' => $criteria,
            'addsubcategories' => 1
        ]);

        if (empty($categories)) {
            return [];
        }

        usort($categories, function ($a, $b){
           if ($a['id'] === $b['id']){
               return 0;
           }
           return $a['id'] > $b['id'] ? 1 : 0;
        });

        return ArrayHelper::map($categories, 'id', 'name');
    }

    public function addMoodleCourse()
    {
        $moodleRest = Yii::$app->moodleRest;
        $data = $moodleRest->request('core_course_create_courses', [
            'courses' => [$this]
        ]);

        return $data;
    }

    public function updateMoodleCourse()
    {
        $moodleRest = Yii::$app->moodleRest;
        $data = $moodleRest->request('core_course_update_courses', [
            'courses' => [$this]
        ]);

        return $data;
    }

    public static function createTree($data)
    {
        $parents = [];
        foreach ($data as $key => $item) {
            $parents[$item['parent']][$item['id']] = $item;
        }
        $treeElem[Settings::getValue('main-moodle-category')] = (object)[
            'id' => $parents[0][Settings::getValue('main-moodle-category')]['id'],
            'text' => $parents[0][Settings::getValue('main-moodle-category')]['name'],
            'type' => 'category',
            'inc' => []
        ];
        self::generateElemTree($treeElem, $parents);
        return $treeElem;
    }

    /**
     * @param $treeElem
     * @param $parents
     * Генерируем элементы дерева с учётом удобного вывода потомков
     */
    private static function generateElemTree(&$treeElem, $parents)
    {
        foreach ($treeElem as $key => $item) {
            $incCourse = [];
            $courses = self::getCourseByCategory($item->id);
            foreach ($courses as $course){
                $incCourse[$course['id']] = (object)[
                    'id' => $course['id'],
                    'text' => $course['fullname'],
                    'type' => 'course'
                ];
            }

            if (empty($item->inc)) {
                $treeElem[$key]->inc = $incCourse;
            }

            if (array_key_exists($key, $parents)) {
                $inc = [];
                foreach ($parents[$key] as $category) {
                    $inc[$category['id']] = (object)[
                        'id' => $category['id'],
                        'text' => $category['name'],
                        'type' => 'category',
                        'inc' => []
                    ];
                }

                $treeElem[$key]->inc = ArrayHelper::merge($inc, $incCourse);
                self::generateElemTree($treeElem[$key]->inc, $parents);
            }
        }
    }

    /**
     * @return mixed
     */
    public static function getHierarchyMoodleCategories()
    {
        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;

        $mCategories = $moodleRest->request('core_course_get_categories', [
            'criteria' => [
                [
                    'key' => 'id',
                    'value' => Settings::getValue('main-moodle-category')
                ],

            ],
        ]);

        return self::createTree($mCategories);
    }

    /**
     * @param $data
     * Рендерим вид
     * @param array $value
     * @param string $parentId
     * @param int $depth
     */
    public static function renderCategoryTree($data, $value = [], $parentId = '', $depth = 1) {
        if(is_array($data)):
            foreach ($data as $item):
                $class = "l{$depth}";
                $selected = '';
                $cat = '';
                if (isset($item->inc) && count($item->inc) > 0) {
                    $class .= ' non-leaf';
                }

                if ($item->type === 'course' && in_array($item->id, $value, true)) {
                    $selected = 'selected';
                }

                if ($item->type === 'category'){
                    $cat = 'category';
                }

                echo "<option class=\"{$class}\" {$selected} data-type=\"{$item->type}\" value=\"{$item->id}\" data-pup=\"{$parentId}\">";
                echo $item->text;
                echo '</option>';
                if(isset($item->inc) && count($item->inc) > 0):
                    self::renderCategoryTree($item->inc, $value, $item->id, $depth + 1);
                endif;
            endforeach;
        endif;
    }

    public static function getCategoryName($catId)
    {
        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;
        $mCategories = $moodleRest->request('core_course_get_categories', [
            'criteria' => [
                [
                    'key' => 'id',
                    'value' => $catId
                ]
            ]
        ]);
        return isset($mCategories[0]) ? $mCategories[0]['name'] : 0;
    }

    public static function getCourseByCategory($catId){

        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;

        $mCourses = $moodleRest->request('core_course_get_courses_by_field', [
            'field' => 'category',
            'value' => $catId
        ]);

        if (!empty($mCourses['warnings'])) {
            var_dump($mCourses);
            die;
        }

        return $mCourses['courses'];
    }

    public static function getAvailableMoodleCourses()
    {

        return self::getHierarchyMoodleCategories();
    }

    public static function getCourse($id)
    {
        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;

        $mCourses = $moodleRest->request('core_course_get_courses', [
            'options' => [
                'ids' => [$id]
            ]

        ]);

        if (!empty($mCourses['exception'])) {
            var_dump($mCourses);
            die;
        }

        return ['MoodleCourse' => $mCourses[0]];
    }

}
