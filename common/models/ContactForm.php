<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $theme;
    public $category;
    public $contact_phone;
    public $body;
    public $verifyCode;
    public $country;
    public $city;

    const CATEGORY_STUDENT = 1;
    const CATEGORY_PARENT = 2;
    const CATEGORY_PREPOD = 3;
    const CATEGORY_WORKER = 4;
    const CATEGORY_OTHER = 5;

    const SUBJECT_ENIM = 1;
    const SUBJECT_PPIFK = 2;
    const SUBJECT_EIP = 3;
    const SUBJECT_GU = 4;
    const SUBJECT_PI = 5;

    const SCENARIO_CONTACT = 'contact';
    const SCENARIO_APPLICATION = 'application';
    const SCENARIO_CONSULTATION = 'consultation';

    public function scenarios()
    {
        $fields = array_keys($this->attributeLabels());
        return [
            self::SCENARIO_DEFAULT => $fields,
            self::SCENARIO_APPLICATION => $fields,
            self::SCENARIO_CONSULTATION => $fields,
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'contact_phone'], 'required', 'on' => self::SCENARIO_CONSULTATION],
            [['name', 'email', 'body'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['name', 'email', 'contact_phone'], 'required', 'on' => self::SCENARIO_APPLICATION],
            [['name', 'email', 'category', 'contact_phone', 'body'], 'string'],
            [['subject', 'contact_phone', 'theme'], 'safe'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
//            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия Имя Отчество',
            'email' => 'Адрес электронной почты',
            'subject' => 'Институт',
            'theme' => 'Тема письма',
            'category' => 'Категория',
            'contact_phone' => 'Контактный телефон',
            'body' => 'Текст обращения',
            'country' => 'Страна',
            'city' => 'Город',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {

        if ($this->validate()) {
            $mailText = 'ФИО: ' . $this->name . PHP_EOL;
            $mailText .= $this->email !== null ? 'e-mail: ' . $this->email . PHP_EOL : '';
            $mailText .= $this->contact_phone !== null ? 'Контактный телефон: ' . $this->contact_phone . PHP_EOL : '';
            $mailText .= $this->country !== null ? 'Страна: ' . $this->country . PHP_EOL : '';
            $mailText .= $this->city !== null ? 'Город: ' . $this->city . PHP_EOL : '';
            $mailText .= $this->body !== null ? 'Текст обращения: ' . $this->body . PHP_EOL : '';

            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setSubject($this->theme)
                ->setTextBody($mailText)
                ->send();

            return true;
        }
        return false;
    }


    static function getCategoryList()
    {
        return [
            self::CATEGORY_STUDENT => 'Студент',
            self::CATEGORY_PARENT => 'Родитель',
            self::CATEGORY_PREPOD => 'Преподаватель',
            self::CATEGORY_WORKER => 'Сотрудник',
            self::CATEGORY_OTHER => 'Другое',
            ];
    }

    static function getSubjectList()
    {
        return [
            self::SUBJECT_ENIM => 'Институт естественных наук и математики ',
            self::SUBJECT_PPIFK => 'Институт педагогики, психологии и физической культуры',
            self::SUBJECT_EIP => 'Институт экономики и права',
            self::SUBJECT_GU => 'Гуманитарный институт',
            self::SUBJECT_PI => 'Политехнический институт',
            ];
    }

    public function getSubjectName()
    {
        if (empty($this->subject)) {
            return 'не выбран';
        }
        return self::getSubjectList()[$this->subject];
    }

    public function getCategoryName()
    {
        if (empty($this->category)) {
            return 'не выбран';
        }
        return self::getCategoryList()[$this->category];
    }
}
