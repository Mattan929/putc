<?php

namespace common\models;

use floor12\files\models\File;
use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $title
 * @property string $value
 * @property string $additional_value
 * @property int|null $cant_be_removed
 * @property string|null $code
 * @property File[] $documents
 * @property File[] $application
 * @property File[] $confirmation
 * @property File[] $contract_offer
 */
class Settings extends \yii\db\ActiveRecord
{
    function behaviors()
    {
        return [
            'files' => [
                'class' => 'floor12\files\components\FileBehaviour',
                'attributes' => [
                    'documents',
                    'application',
                    'confirmation',
                    'contract_offer',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'value'], 'required'],
            [['value', 'additional_value'], 'string'],
            [['cant_be_removed'], 'integer'],
            ['documents', 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif', 'docx', 'doc', 'pdf'], 'maxFiles' => 15],
            [['contract_offer', 'application', 'confirmation'], 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif', 'docx', 'doc', 'pdf'], 'maxFiles' => 1],
            [['title', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'value' => 'Значение',
            'additional_value' => 'Дополнительное значение',
            'cant_be_removed' => 'Без возможности удалить',
            'code' => 'Код',
            'documents' => 'Документы',
            'contract_offer' => 'Договор офферты',
            'application' => 'Пример заявления',
            'confirmation' => 'Пример согласия на обработку персональных данных',
        ];
    }

    public static function getValue($code)
    {
        $setting = self::findOne(['code' => $code]);
        if (!empty($setting)) {
            return $setting->value;
        }
        return '';
    }

    public static function getSiteDocuments()
    {
        $setting = self::findOne(['code' => 'documents']);
        if (!empty($setting)) {
            return $setting->documents;
        }
        return [];
    }
}
