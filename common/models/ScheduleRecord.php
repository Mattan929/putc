<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "schedule_record".
 *
 * @property int $id
 * @property int $day_id
 * @property int $time_id
 * @property int $schedule_id
 * @property int $subject_id
 * @property string|null $address
 * @property string|null $classroom
 *
 * @property Schedule $schedule
 * @property Subject $subject
 */
class ScheduleRecord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schedule_record';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['day_id', 'time_id', 'schedule_id', 'subject_id'], 'required'],
            [['address_id', 'day_id', 'time_id', 'schedule_id', 'subject_id'], 'integer'],
            [['classroom'], 'string', 'max' => 255],
            [['schedule_id'], 'exist', 'skipOnError' => true, 'targetClass' => Schedule::className(), 'targetAttribute' => ['schedule_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day_id' => 'День',
            'time_id' => 'Время',
            'schedule_id' => 'Расписание',
            'subject_id' => 'Предмет',
            'address_id' => 'Адрес',
            'classroom' => 'Кабинет',
        ];
    }

    /**
     * Gets query for [[Schedule]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSchedule()
    {
        return $this->hasOne(Schedule::className(), ['id' => 'schedule_id']);
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    public static function getDayList()
    {
        return [
            1 => 'Понедельник',
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
            7 => 'Воскресенье',
        ];
    }

    public static function getTimeList()
    {
        return ArrayHelper::map(ScheduleTime::findAll(['status' => ScheduleTime::STATUS_ACTIVE]), 'id', 'time');
    }

    public static function getAddressList()
    {
        return ArrayHelper::map(ScheduleAddress::findAll(['status' => ScheduleAddress::STATUS_ACTIVE]), 'id', 'address');
    }

    public function getAddress()
    {
        return $this->hasOne(ScheduleAddress::className(), ['id' => 'address_id']);
    }

    public function getTime()
    {
        return $this->hasOne(ScheduleTime::className(), ['id' => 'time_id']);
    }
}
