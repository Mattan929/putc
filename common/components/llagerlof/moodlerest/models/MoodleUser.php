<?php

namespace common\components\llagerlof\moodlerest\models;

use common\components\llagerlof\moodlerest\MoodleRest;
use Yii;
use yii\web\HttpException;

class MoodleUser
{
    public $auth = 'manual';
    public $username;
    public $firstname;
    public $lastname;
    public $createpassword = 0;
    public $email;
    public $idnumber;
    public $lang = 'ru';
    public $calendartype = 'gregorian';

    /**
     * @return mixed
     */
    public function create()
    {
        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;

        $response = $moodleRest->request('core_user_create_users', [
            'users' => [$this]
        ]);

        if (isset($response['exception'])) {
            throw new HttpException(500, $response['message']);
        }

        return $response;
    }

}