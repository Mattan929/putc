<?php

namespace common\components\llagerlof\moodlerest\models;

use common\components\llagerlof\moodlerest\MoodleRest;
use Yii;
use yii\web\HttpException;

class MoodleGroup
{
    public $courseid;//id of course(require)
    public $name;//multilang compatible name, course unique(require)
    public $description;//group description text(require)
    public $descriptionformat = 1;//description format (1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN)(require, default - 1)
    public $idnumber;//id number
    public $enrolmentkey;//group enrol secret phrase

    /**
     * @return mixed
     */
    public function create()
    {
        /** @var MoodleRest $moodleRest */
        $moodleRest = Yii::$app->moodleRest;

        $response = $moodleRest->request('core_group_create_groups', [
            'groups' => [$this]
        ]);

        if (isset($response['exception'])) {
            throw new HttpException(500, $response['message']);
        }

        return $response;
    }

}