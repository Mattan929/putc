<?php
return [
    'adminEmail' => 'cdp@kgsu.ru',
    'supportEmail' => 'cdp@kgsu.ru',
    'senderEmail' => 'noreply@kgsu.ru',
    'senderName' => 'ЦЕНТР ДОВУЗОВСКОЙ ПОДГОТОВКИ',
    'user.passwordResetTokenExpire' => 3600,
];
