<?php
return [
    'name' => 'Центр довузовской подготовки КГУ',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'ru_RU',
    'bootstrap' => [
        'dektrium\user\Bootstrap',
        'dektrium\rbac\Bootstrap',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => yii\helpers\ArrayHelper::merge(
        require( dirname(dirname(__DIR__)) . '/vendor/yiisoft/extensions.php'),
        [
            'dektrium/yii2-rbac' => [
                'name' => 'dektrium/yii2-rbac',
                'version' => '1.0.1',
                'alias' => [
                    '@dektrium/rbac' => '@dektrium/rbac',
                ],
            ],
            'dektrium/yii2-user' => [
                'name' => 'dektrium/yii2-user',
                'version' => '0.9.14.0',
                'alias' => [
                    '@dektrium/user' => '@dektrium/user',
                ],
                'bootstrap' => 'dektrium\\user\\Bootstrap',
            ],
        ]
    ),
    'components' => [
        'moodleRest' => [
            'class' => common\components\llagerlof\moodlerest\MoodleRest::class,
            'serverAddress' => 'https://de.kgsu.ru/webservice/rest/server.php',
            'token' => '8a9cc63103b88dc45ee8279fbae3c9cf',
            'returnFormat' => common\components\llagerlof\moodlerest\MoodleRest::RETURN_ARRAY
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'forceCopy' => true
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableConfirmation' => true,
            'enableGeneratingPassword' => false,
            'admins' => ['administrator'],
            'adminPermission' => 'superadmin',
            'mailer' => [
                'sender'                => ['cdp@kgsu.ru' => 'ЦЕНТР ДОВУЗОВСКОЙ ПОДГОТОВКИ'],
//                'welcomeSubject'        => 'Welcome subject',
//                'confirmationSubject'   => 'Confirmation subject',
//                'reconfirmationSubject' => 'Email change subject',
//                'recoverySubject'       => 'Recovery subject',
            ],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
            'adminPermission' => 'superadmin'
        ],
    ],
];
