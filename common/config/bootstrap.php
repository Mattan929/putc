<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@dektrium/user', dirname(dirname(__DIR__)) . '/common/modules/dektrium/yii2-user');
Yii::setAlias('@dektrium/rbac', dirname(dirname(__DIR__)) . '/common/modules/dektrium/yii2-rbac');
Yii::setAlias('@floor12/files', dirname(__DIR__) . '/modules/floor12/yii2-module-files/src');
Yii::setAlias('@dominus77/owlcarousel2', dirname(dirname(__DIR__)) . '/common/widgets/yii2-owl-carousel-2');
