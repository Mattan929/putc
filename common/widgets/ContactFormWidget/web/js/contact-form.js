if(typeof mattan929 == "undefined" || !mattan929) {
    var mattan929 = {};
}

mattan929.contactForm = {
    timeoutId: {},

    init: function () {

        $(document).on('beforeSubmit', '[data-role=kgsu-feedback-form]', function (e) {
            if (!$(this).find('[data-role=person-data-confirm]')[0].checked){
                clearTimeout(mattan929.contactForm.timeoutId);
                $(this).find('[data-role=person-data-confirm]').closest('label').addClass('red');
                mattan929.contactForm.timeoutId = setTimeout(function () {
                    $(document).find('.red [data-role=person-data-confirm]').closest('label').removeClass('red');
                }, 2000);
                return false;
            }
        });
    }
},

mattan929.contactForm.init();