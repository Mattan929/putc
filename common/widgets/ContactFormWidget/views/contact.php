<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $send_to string */
/* @var $model \common\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'kgsu-contact-form',
                'options' => [
                    'data-role' => 'kgsu-feedback-form',
                ],
                'action' => \yii\helpers\Url::to(['/index/contact', 'send_to' => $send_to])
            ]); ?>

            <?= $form->field($model, 'name')->textInput()->label('Фамилия Имя Отчество <span class="red">*</span>') ?>

            <?= $form->field($model, 'email')->label('Адрес электронной почты <span class="red">*</span>') ?>

            <?= $form->field($model, 'contact_phone') ?>

            <?= $form->field($model, 'subject')->dropDownList(\common\models\ContactForm::getSubjectList(), ['prompt' => '-']) ?>

            <?= $form->field($model, 'theme')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'scenario')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'category')->dropDownList(\common\models\ContactForm::getCategoryList())->label('Категория <span class="red">*</span>') ?>

            <?= $form->field($model, 'body')->textarea(['rows' => 6])->label('Текст обращения <span class="red">*</span>') ?>
        </div>
        <div class="col-md-12">
            <label for="person-data">
                <input id="person-data" type="checkbox">
                Я подтверждаю согласие на обработку своих персональных данных, а именно совершение действий,
                предусмотренных п. 3 ч. 1 ст. 3 Федерального закона от 27.07.2006 N 152-ФЗ "О персональных данных", и
                подтверждаю, что, давая такое согласие, действую свободно, своей волей и в своем интересе.
            </label>
            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-default', 'name' => 'contact-button']) ?>
            </div>
            <span class="red">*</span> - обязательные поля
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
