<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $send_to string */

/* @var $model \common\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>
<div class="row">
    <div class="col-md-12">
        <h3>
            Хочешь узнать еще больше об инновационных способах подготовки к ЕГЭ?
        </h3>
    </div>
    <?php
    $template = "{beginWrapper}\n{input}\n{endWrapper}";
    $form = ActiveForm::begin([
        'id' => 'kgsu-consultation-form',
        'options' => [
            'data-role' => 'kgsu-feedback-form',
        ],
        'action' => \yii\helpers\Url::toRoute(['contact', 'send_to' => $send_to]),
        'fieldConfig' => [
            'template' => $template,
        ]
    ]);
    ?>

    <?= $form->field($model, 'theme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'scenario')->hiddenInput()->label(false) ?>

    <div class="col-md-4">
        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'contact_phone')->textInput(['placeholder' => 'Телефон'])->label(false) ?>
    </div>
    <div class="col-md-4">
        <div class="form-group text-center">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-putc', 'name' => 'contact-button']) ?>
        </div>
    </div>
    <div class="col-md-12">
        <label for="person-data-inline">
            <input id="person-data-inline" data-role="person-data-confirm" type="checkbox">
            Я даю согласие на обработку персональных данных, согласен на получение информационных рассылок от Центра
            довузовской подготовки Курганского государственного университета и соглашаюсь с политикой
            конфиденциальности.
        </label>
    </div>

    <?php ActiveForm::end(); ?>
</div>

