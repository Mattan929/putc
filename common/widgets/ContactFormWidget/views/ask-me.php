<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $send_to string */

/* @var $model \common\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>
<div class="site-ask-me">

    <?php
    $template = "{beginWrapper}\n{input}\n{endWrapper}";
    $form = ActiveForm::begin([
        'id' => 'kgsu-asc-me-form',
        'options' => [
            'data-role' => 'kgsu-feedback-form',
        ],
        'action' => \yii\helpers\Url::toRoute(['contact', 'send_to' => $send_to]),
        'fieldConfig' => [
            'template' => $template,
        ]
    ]); ?>

    <div class="col-lg-6">

        <?= $form->field($model, 'name')->textInput(['placeholder' => '*Введите ФИО'])->label(false) ?>

        <?= $form->field($model, 'email')->textInput(['placeholder' => '*Введите адрес электронной почты'])->label(false) ?>

        <?= $form->field($model, 'contact_phone')->textInput(['placeholder' => 'Введите номер телефона'])->label(false) ?>

        <?= $form->field($model, 'theme')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'scenario')->hiddenInput()->label(false) ?>

    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'body')->textarea(['rows' => 6, 'placeholder' => '*Введите свой вопрос'])->label(false) ?>
    </div>
    <div class="col-md-12">
        <label for="person-data-ask-me">
            <input id="person-data-ask-me" data-role="person-data-confirm" type="checkbox">
            Я подтверждаю согласие на обработку своих персональных данных, а именно совершение действий,
            предусмотренных п. 3 ч. 1 ст. 3 Федерального закона от 27.07.2006 N 152-ФЗ "О персональных данных", и
            подтверждаю, что, давая такое согласие, действую свободно, своей волей и в своем интересе.
        </label>
        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-putc', 'name' => 'contact-button']) ?>
            <span>*</span> - обязательные поля
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
