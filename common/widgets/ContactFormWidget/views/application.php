<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $send_to string */

/* @var $model \common\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">

    <div class=" col-md-offset-3 col-md-6">
        <?php
        $template = "{beginWrapper}\n{input}\n{endWrapper}";
        $form = ActiveForm::begin([
            'id' => 'kgsu-application-form',
            'options' => [
                'data-role' => 'kgsu-feedback-form',
            ],
            'action' => \yii\helpers\Url::to(['/index/contact', 'send_to' => $send_to]),
            'fieldConfig' => [
                'template' => $template,
            ]
        ]);
        ?>

        <?= $form->field($model, 'theme')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'scenario')->hiddenInput()->label(false) ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>

                <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail'])->label(false) ?>

                <?= $form->field($model, 'contact_phone')->textInput(['placeholder' => 'Телефон'])->label(false) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'country')->textInput(['placeholder' => 'Страна'])->label(false) ?>

                <?= $form->field($model, 'city')->textInput(['placeholder' => 'Город'])->label(false) ?>
            </div>
        </div>
        <div class="row text-center">
            <label for="person-data">
                <input id="person-data" type="checkbox">
                <a href="#">Согласен с политикой конфиденциальности и даю согласие на обработку персональных данных</a>
            </label>
            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

