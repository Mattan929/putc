<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $send_to string */

/* @var $model \common\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div style="position: relative; height: 0">
    <div class="panel call-me-panel">
        <div class="panel-header text-center">
            <h3>Записывайся
                на консультацию</h3>
        </div>
        <div class="panel-body text-center">
            <?php
            $template = "{beginWrapper}\n{input}\n{endWrapper}";
            $form = ActiveForm::begin([
                'id' => 'kgsu-consultation-form1',
                'options' => [
                    'data-role' => 'kgsu-feedback-form',
                ],
                'action' => \yii\helpers\Url::toRoute(['contact', 'send_to' => $send_to]),
                'fieldConfig' => [
                    'template' => $template,
                ]
            ]);
            ?>
            <?= $form->field($model, 'theme')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'scenario')->hiddenInput()->label(false) ?>
            <div class="form-group">
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'contact_phone')->textInput(['placeholder' => 'Телефон'])->label(false) ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Записаться', ['class' => 'btn btn-putc form-control', 'name' => 'contact-button']) ?>
            </div>
            <div class="form-group">
                <label for="person-data">
                    <input id="person-data" data-role="person-data-confirm" type="checkbox">
                    Я даю согласие на обработку
                    персональных данных, согласен
                    на получение информационных
                    рассылок от Центра довузовской подготовки
                    Курганского государственного университета
                    и соглашаюсь с политикой
                    конфиденциальности.
                </label>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


